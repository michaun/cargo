cmake_minimum_required (VERSION 3.7)
project (Cargo C)

find_package(PkgConfig REQUIRED)
pkg_check_modules(PANGO REQUIRED pangocairo)


add_executable (Cargo src/main.c src/SDLManager.c src/Title.c src/Scrolling.c src/ControlManager.c src/ResourceManager.c src/ChapterOne.c src/SLList.c src/GameStateManager.c src/MissileManager.c src/PickableManager.c src/ExplosionManager.c src/FlyingManager.c src/Behaviors.c src/CommonManager.c src/Collisions.c src/CairoArt.c src/ListManager.c src/MapsManager.c src/TextDB.c)
target_compile_options(Cargo PRIVATE -g -Wall -g)
target_include_directories(Cargo PUBLIC ${PANGO_INCLUDE_DIRS})
target_link_libraries(Cargo -lSDL2 -lSDL2_mixer -lm ${PANGO_LIBRARIES})

install(TARGETS Cargo RUNTIME DESTINATION bin COMPONENT applications)
