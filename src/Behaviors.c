/*
    This file is a part of cargo - a space shooter game
    Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//==============================================================================
#include "Behaviors.h"
#include "FlyingManager.h"

static void AttackNormal(SLList * who)//(SLList * who)
{
  Flying * o = ((Flying *) who->data);
  SDL_Rect p = o->o.position;
  SDL_Point pos;
      pos.x = p.x + p.w/2;
      pos.y = p.y + 10;
      o->fm->mm->Add(o->fm->mm, *o->o.resource->listhead, who, o->angle, 25, 0, 0, 0, o->associatedMissileList, pos, MISSILE);
}

static void AttackDouble(SLList * who)
{
  Flying * o = ((Flying *) who->data);
  SDL_Rect p = o->o.position;
  SDL_Point pos;
      pos.x = p.x + p.w;
      pos.y = p.y + 10;
      o->fm->mm->Add(o->fm->mm, *o->o.resource->listhead, who, o->angle, 25, 0, 0, 0, o->associatedMissileList, pos, MISSILE);
      pos.x = p.x + p.w;
      pos.y = p.y + 20;
      o->fm->mm->Add(o->fm->mm, *o->o.resource->listhead, who, o->angle, 25, 0, 0, 0, o->associatedMissileList, pos, MISSILE);
}

static void AttackTriple(SLList * who)
{
  Flying * o = ((Flying *) who->data);
  SDL_Rect p = o->o.position;
  SDL_Point pos;
      pos.x = p.x + p.w;
      pos.y = p.y + 10;
      o->fm->mm->Add(o->fm->mm, *o->o.resource->listhead, who, o->angle-15, 25, 0, 0, 0, o->associatedMissileList, pos, MISSILE);
      pos.x = p.x + p.w;
      pos.y = p.y + 10;
      o->fm->mm->Add(o->fm->mm, *o->o.resource->listhead, who, o->angle, 25, 0, 0, 0, o->associatedMissileList, pos, MISSILE);
      pos.x = p.x + p.w;
      pos.y = p.y + 10;
      o->fm->mm->Add(o->fm->mm, *o->o.resource->listhead, who, o->angle+15, 25, 0, 0, 0, o->associatedMissileList, pos, MISSILE);
}

static void AttackRocket(SLList * who)
{
  Flying * o = ((Flying *) who->data);
  SDL_Rect p = o->o.position;
  SDL_Point pos;
      pos.x = p.x + p.w;
      pos.y = p.y + 10;
      o->fm->mm->Add(o->fm->mm, *o->o.resource->listhead, who, o->angle, 0, 6, 5, 8, o->associatedMissileList, pos, ROCKET);
}

static void AttackBoss(SLList * who)
{
  Flying * o = ((Flying *) who->data);
  SDL_Rect p = o->o.position;
  SDL_Point pos;
      pos.x = p.x + 180;
      pos.y = p.y + 10;
      o->fm->mm->Add(o->fm->mm, *o->o.resource->listhead, who, o->angle, 25, 0, 0, 0, o->associatedMissileList, pos, MISSILE);
      pos.x = p.x + p.w/4 + 15;
      pos.y = p.y + 35;
      o->fm->mm->Add(o->fm->mm, *o->o.resource->listhead, who, o->angle, 25, 0, 0, 0, o->associatedMissileList, pos, MISSILE);
      pos.x = p.x + 15;
      pos.y = p.y + (p.h/4) + 55;
      o->fm->mm->Add(o->fm->mm, *o->o.resource->listhead, who, o->angle, 25, 0, 0, 0, o->associatedMissileList, pos, MISSILE);
      pos.x = p.x + 15;
      pos.y = p.y + (p.h/4) + 55;
      o->fm->mm->Add(o->fm->mm, *o->o.resource->listhead, who, o->angle-15, 25, 0, 0, 0, o->associatedMissileList, pos, MISSILE);
      pos.x = p.x + 15;
      pos.y = p.y + (p.h/4) + 55;
      o->fm->mm->Add(o->fm->mm, *o->o.resource->listhead, who, o->angle+15, 25, 0, 0, 0, o->associatedMissileList, pos, MISSILE);
      pos.x = p.x + 2*(p.w/4);
      pos.y = p.y + 3*(p.h/4);
      o->fm->mm->Add(o->fm->mm, *o->o.resource->listhead, who, o->angle, 0, 6, 8, 5, o->associatedMissileList, pos, ROCKET);
}

void ShootingScheme(SLList * who)
{
  Flying * o = ((Flying *) who->data);
  
  if(!o->weaponTimeout) {
    switch(o->attackType) {
    default:
    case ATTACK_NONE:
      // no attack
      break;
    case ATTACK_NORMAL:
      AttackNormal(who);
      break;
    case ATTACK_DOUBLE:
      AttackDouble(who);
      break;
    case ATTACK_TRIPLE:
      AttackTriple(who);
      break;
    case ATTACK_ROCKET:
      AttackRocket(who);
      break;
    case ATTACK_BOSS1:
      AttackBoss(who);
      break;
    }
    o->weaponTimeout = o->weaponReloadTime;
  }
}

static void BehaviorHero(SLList * thisObject)
{
  Flying * current = ((Flying *)thisObject->data);
  current->o.position.x += 5;
  if(abs(current->o.position.x - current->desiredPosition.x) <= 5) {
    current->behavior = NO_BEHAVIOR; // give the power back
  }
}

static void BehaviorWreckage(SLList * thisObject)
{
  Flying * current = ((Flying *)thisObject->data);
  current->o.position.x -= FLYING_SPEED;
}

static void BehaviorGate(SLList * thisObject)
{
  Flying * current = ((Flying *)thisObject->data);
  current->o.position.x -= 5;
}

static void BehaviorFlyBy(SLList * thisObject)
{
  Flying * current = ((Flying *)thisObject->data);
  if(rand() % 100 < 5) ShootingScheme(thisObject);
  current->o.position.x -= FLYING_SPEED;
}

static void BehaviorEnemy3(SLList * thisObject)
{
  Flying * current = ((Flying *)thisObject->data);
  if(rand() % 100 < 5) ShootingScheme(thisObject);
  if(abs(current->o.position.y - current->desiredPosition.y) > 5)
    (current->o.position.y < current->desiredPosition.y) ? (current->o.position.y += 5) : (current->o.position.y -= 5);
  if(abs(current->o.position.x - current->desiredPosition.x) > 5)
    (current->o.position.x < current->desiredPosition.x) ? (current->o.position.x += 5) : (current->o.position.x -= 5);
}

static void BehaviorEnemy2(SLList * thisObject)
{
  Flying * current = ((Flying *)thisObject->data);
  current->o.position.x -= 25;
}

static void BehaviorEnemy1(SDLManager *g, SLList * thisObject)
{
  Flying * current = ((Flying *)thisObject->data);
  current->o.position.x -= 10;
  if(rand() % 100 < 5) ShootingScheme(thisObject);
  //current->weaponTimeout = 0; // OVERRIDE ! WE ARE LEGION ! NO TIMEOUT !
  if(abs(current->o.position.y - current->desiredPosition.y) <= 5) current->desiredPosition.y = rand() % g->windowSize.h - current->o.position.h;
  (current->o.position.y < current->desiredPosition.y) ? (current->o.position.y += 5) : (current->o.position.y -= 5);
  if(current->desiredPosition.y < TOP_OFFSET) current->desiredPosition.y = TOP_OFFSET;
  if(current->desiredPosition.y > g->windowSize.h - current->o.position.h) current->desiredPosition.y = g->windowSize.h - current->o.position.h;
}

static void BehaviorEnemyWave(SDLManager *g, SLList * thisObject)
{
  Flying * current = ((Flying *)thisObject->data);
  char nextdirection = 0;
  current->o.position.x -= 10;
  if(rand() % 100 < 5) ShootingScheme(thisObject);
  if(current->o.position.y < current->desiredPosition.y) {
    current->o.position.y += 5;
    nextdirection = 0;
  } else {
    current->o.position.y -= 5;
    nextdirection = 1;
  }
  if(abs(current->o.position.y - current->desiredPosition.y) <= 5)
    (nextdirection) ? (current->desiredPosition.y = current->o.position.y + g->windowSize.h/4) : (current->desiredPosition.y = current->o.position.y - g->windowSize.h/4);
  if(current->desiredPosition.y < TOP_OFFSET) current->desiredPosition.y = TOP_OFFSET;
  if(current->desiredPosition.y > g->windowSize.h - current->o.position.h) current->desiredPosition.y = g->windowSize.h - current->o.position.h;
}


static void BehaviorEnemyUp(SLList * thisObject)
{
  Flying * current = ((Flying *)thisObject->data);
  current->o.position.x -= 15;
  current->o.position.y -= 2;
  if(current->o.position.y % 16 == 0) ShootingScheme(thisObject);
}

static void BehaviorEnemyDown(SLList * thisObject)
{
  Flying * current = ((Flying *)thisObject->data);
  current->o.position.x -= 15;
  current->o.position.y += 2;
  if(current->o.position.y % 16 == 0) ShootingScheme(thisObject);
}

static void BehaviorBoss1(SDLManager *g, SLList * thisObject)
{
  Flying * current = ((Flying *)thisObject->data);
  current->o.position.x -= 5;
  if(rand() % 50 < 4) ShootingScheme(thisObject);
  /*
    current->desiredPosition.y = 100;
    current->desiredPosition.x = 100;
    current->o.position.y = 100;
    current->o.position.x = 100;
  */
  if(abs(current->o.position.y - current->desiredPosition.y) <= 5) current->desiredPosition.y = rand() % g->windowSize.h - current->o.position.h;
  if(abs(current->o.position.x - current->desiredPosition.x) <= 5) current->desiredPosition.x = g->windowSize.w/2 + (rand() % g->windowSize.w/2) - current->o.position.w;
  (current->o.position.y < current->desiredPosition.y) ? (current->o.position.y += 5) : (current->o.position.y -= 5);
  (current->o.position.x < current->desiredPosition.x) ? (current->o.position.x += 10) : (current->o.position.x -= 10);
  if(current->desiredPosition.y < TOP_OFFSET) current->desiredPosition.y = TOP_OFFSET;
  if(current->desiredPosition.y > g->windowSize.h - current->o.position.h) current->desiredPosition.y = g->windowSize.h - current->o.position.h;
  if(current->desiredPosition.x < g->windowSize.w/2) current->desiredPosition.x = g->windowSize.w/2;
}

/*
static void DropLootAfterDeathBehavior(SDLManager *g, ChapterOne *t, SLList * thisObject)
{
  Flying * currentenemy = (Flying *) thisObject;
  t->pm->AddXY(g, t->chapteronetextures, &t->pickables, rand() % NUM_PICKABLES,
              currentenemy->o.position.x + currentenemy->o.position.w/2,
              currentenemy->o.position.y + currentenemy->o.position.h/2);

  //DestroyedObjectBehavior(g, t, thisObject);
}
*/

static void DestroyedObjectBehavior(SLList * thisObject)
{
  SLList * hiterator = NULL;
  if(((Flying *)thisObject->data)->leaveWreckage != 1) {
    thisObject->markForDeletion = 1;
  } else {
    // last frame is the wreckage!
    while(((Flying *)thisObject->data)->o.currentFrame->next != NULL) ((Flying *)thisObject->data)->o.currentFrame = ((Flying *)thisObject->data)->o.currentFrame->next;
    ((Flying *)thisObject->data)->behavior = BEHAVIOR_WRECKAGE; // behavior is generally "just fly with the speed of space moving"
    for(hiterator = ((Flying *)thisObject->data)->o.hitbox; hiterator; hiterator = hiterator->next)
      ((Hitbox *)hiterator->data)->damagable = 0; // this is a wreck now, you cannot shoot it
  }
}

static void AfterDeath87(SDLManager * g, SLList * thisObject)
{
  SLList * rog;
  Flying * restofthegroup;
  Flying * current = ((Flying *)thisObject->data);
  rog = current->fm->Get(*((Flying*)thisObject->data)->listhead, 88);
  if(rog) {
    restofthegroup = ((Flying*)rog->data);
    restofthegroup->desiredPosition.x = 3*(g->windowSize.w/4);
    restofthegroup->desiredPosition.y = 1* (g->windowSize.h/4);
  }
  // at this point we are done with specific behavior, proceed to the usual stuff
  DestroyedObjectBehavior(thisObject); 
}

static void DestroyedWallBehavior66(SDLManager * g, SLList * thisObject)
{
  Flying * current = ((Flying *)thisObject->data);
  SLList * walls;
  walls = current->fm->Get(*((Flying*)thisObject->data)->listhead, 67);
  if(walls)
    walls->markForDeletion = 1;
  walls = current->fm->Get(*((Flying*)thisObject->data)->listhead, 68);
  if(walls)
    walls->markForDeletion = 1;
  // at this point we are done with specific behavior, proceed to the usual stuff
  DestroyedObjectBehavior(thisObject); 
}


void BehaviorSelect(enum BehaviorScheme bt, SDLManager * g, SLList * thisObject)
{
  switch(bt) {
  case BEHAVIOR_HERO:
    BehaviorHero(thisObject);
    break;
  case BEHAVIOR_WRECKAGE:
    BehaviorWreckage(thisObject);
    break;
  case BEHAVIOR_GATE:
    BehaviorGate(thisObject);
    break;
  case BEHAVIOR_FLY_BY:
    BehaviorFlyBy(thisObject);
    break;
  case BEHAVIOR_ENEMY3:
    BehaviorEnemy3(thisObject);
    break;
  case BEHAVIOR_ENEMY2:
    BehaviorEnemy2(thisObject);
    break;
  case BEHAVIOR_ENEMY1:
    BehaviorEnemy1(g, thisObject);
    break;
  case BEHAVIOR_ENEMY_WAVE:
    BehaviorEnemyWave(g, thisObject);
    break;
  case BEHAVIOR_ENEMY_UP:
    BehaviorEnemyUp(thisObject);
    break;
  case BEHAVIOR_ENEMY_DOWN:
    BehaviorEnemyDown(thisObject);
    break;
  case BEHAVIOR_BOSS1:
    BehaviorBoss1(g, thisObject);
    break;  
  case DESTROYED_OBJECT_BEHAVIOR:
    DestroyedObjectBehavior(thisObject);
    break;
  case AFTER_DEATH87:
    AfterDeath87(g, thisObject);
    break;
  case DESTROYED_WALL_BEHAVIOR66:
    DestroyedWallBehavior66(g, thisObject);
    break;
  case NO_BEHAVIOR:
  case COUNT_BEHAVIORS:
  default:
    break;
  }
}
