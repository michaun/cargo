/*
    This file is a part of cargo - a space shooter game
    Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _BEHAVIORS_H_
#define _BEHAVIORS_H_

#include "SDLManager.h"
#include "SLList.h"
#include "FlyingManager.h"


void ShootingScheme(SLList * who);
void BehaviorSelect(enum BehaviorScheme bt, SDLManager * g, SLList * thisobject);
  
#endif // _BEHAVIORS_H_
