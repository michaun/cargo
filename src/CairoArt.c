/*
  This file is a part of cargo - a space shooter game
  Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//==============================================================================
#include "CairoArt.h"
#include <pango/pangocairo.h>


SDL_Texture * CreateCairoShipTexture(SDLManager * g)
{
  SDL_Surface * surfTemp = NULL;
  SDL_Surface * surfTemp2 = NULL;
  SDL_Texture * textReturn = NULL;
  
  surfTemp = SDL_CreateRGBSurface ( 0, 70, 30, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
  SDL_LockSurface(surfTemp);
  cairo_surface_t *surface = cairo_image_surface_create_for_data ( surfTemp->pixels, CAIRO_FORMAT_ARGB32, surfTemp->w, surfTemp->h, surfTemp->pitch);
  cairo_t *cr = cairo_create (surface);
  
  cairo_scale(cr, 0.2, 0.15);
  cairo_set_source_rgb(cr, 1.0, 0.0, 0.0);
  cairo_arc(cr, 145, 90, 50, 235 * (M_PI/180), 320 * (M_PI/180));
    
  cairo_curve_to(cr, 184, 57, 190, 105, 260, 125);
  cairo_arc(cr, 230, 10, 120, 75 * (M_PI/180), 160 * (M_PI/180));
  cairo_close_path (cr);
  cairo_fill_preserve(cr); // we need preserve to preserve contour lines
  cairo_stroke(cr);

  // ship
  //back section
  cairo_arc(cr, 110, 100, 90, 100 * (M_PI/180), 270 * (M_PI/180));
  //middle section
  cairo_arc_negative(cr, 230, 10, 120, 180 * (M_PI/180), 65 * (M_PI/180));
           
  //front section
  cairo_rel_line_to (cr, 60, 50);
  cairo_rel_line_to (cr, -80, 20);
  cairo_rel_line_to (cr, -60, -20);
  cairo_rel_line_to (cr, 30, 10);
  cairo_rel_line_to (cr, -50, 20);
  cairo_curve_to (cr, 185, 200, 150, 150, 100, 170);
  cairo_close_path (cr);
    
  cairo_set_line_width(cr, 2.0);
    
  cairo_set_source_rgb(cr, 0.84, 0.93, 0.77);
    
  cairo_fill_preserve(cr); // we need preserve to preserve contour lines
  cairo_set_source_rgb(cr, 0.56, 0.61, 0.51);
  cairo_arc(cr, 210, 215, 120, 193 * (M_PI/180), 230 * (M_PI/180));
  cairo_stroke(cr);

  // cannon
  cairo_set_source_rgb(cr, 0.5, 0.5, 0.5);
  cairo_arc (cr, 145.0, 140.0, 12, 0, 2 * M_PI);
  cairo_stroke(cr);
  cairo_destroy (cr);
  cairo_surface_destroy (surface);
  SDL_UnlockSurface(surfTemp);

  surfTemp2 = SDL_ConvertSurfaceFormat(surfTemp, SDL_PIXELFORMAT_RGBA8888, 0);
  if((textReturn = SDL_CreateTextureFromSurface( g->renderer, surfTemp2 )) == NULL) {
    printf("Error creating texture from surface!");
  }

  SDL_FreeSurface(surfTemp);
  SDL_FreeSurface(surfTemp2);

  return textReturn;
}

//==============================================================================

SDL_Texture * CreateCairoEnemyTexture(SDLManager * g)
{
  SDL_Surface * surfTemp = NULL;
  SDL_Surface * surfTemp2 = NULL;
  SDL_Texture * textReturn = NULL;
  
  surfTemp = SDL_CreateRGBSurface ( 0, 70, 30, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
  SDL_LockSurface(surfTemp);
  cairo_surface_t *surface = cairo_image_surface_create_for_data ( surfTemp->pixels, CAIRO_FORMAT_ARGB32, surfTemp->w, surfTemp->h, surfTemp->pitch);
  cairo_t *cr = cairo_create (surface);
  
  cairo_matrix_t matrix;
  cairo_matrix_init(&matrix,
                    -1.0, 0.0,
                    0.0, 1.0,
                    70.0, 0.0);
  cairo_transform(cr, &matrix);
  cairo_scale(cr, 0.2, 0.15);
  cairo_set_source_rgb(cr, 0.0, 1.0, 0.0);
  cairo_arc(cr, 145, 90, 50, 235 * (M_PI/180), 320 * (M_PI/180));
    
  cairo_curve_to(cr, 184, 57, 190, 105, 260, 125);
  cairo_arc(cr, 230, 10, 120, 75 * (M_PI/180), 160 * (M_PI/180));
  cairo_close_path (cr);
  cairo_fill_preserve(cr); // we need preserve to preserve contour lines
  cairo_stroke(cr);

  // ship
  //back section
  cairo_arc(cr, 110, 100, 90, 100 * (M_PI/180), 270 * (M_PI/180));
  //middle section
  cairo_arc_negative(cr, 230, 10, 120, 180 * (M_PI/180), 65 * (M_PI/180));
           
  //front section
  cairo_rel_line_to (cr, 60, 50);
  cairo_rel_line_to (cr, -80, 20);
  cairo_rel_line_to (cr, -60, -20);
  cairo_rel_line_to (cr, 30, 10);
  cairo_rel_line_to (cr, -50, 20);
  cairo_curve_to (cr, 185, 200, 150, 150, 100, 170);
  cairo_close_path (cr);
    
  cairo_set_line_width(cr, 2.0);
    
  cairo_set_source_rgb(cr, 0.84, 0.93, 0.0);
    
  cairo_fill_preserve(cr); // we need preserve to preserve contour lines
  cairo_set_source_rgb(cr, 0.56, 0.0, 0.0);
  cairo_arc(cr, 210, 215, 120, 193 * (M_PI/180), 230 * (M_PI/180));
  cairo_stroke(cr);

  // cannon
  cairo_set_source_rgb(cr, 0.5, 0.5, 0.5);
  cairo_arc (cr, 145.0, 140.0, 12, 0, 2 * M_PI);
  cairo_stroke(cr);
  cairo_destroy (cr);
  cairo_surface_destroy (surface);
  SDL_UnlockSurface(surfTemp);

  surfTemp2 = SDL_ConvertSurfaceFormat(surfTemp, SDL_PIXELFORMAT_RGBA8888, 0);
  if((textReturn = SDL_CreateTextureFromSurface( g->renderer, surfTemp2 )) == NULL) {
    printf("Error creating texture from surface!");
  }

  SDL_FreeSurface(surfTemp);
  SDL_FreeSurface(surfTemp2);

  return textReturn;
}

//==============================================================================

SDL_Texture * CreateShieldTexture(SDLManager * g)
{
  SDL_Surface * surfTemp = NULL;
  SDL_Surface * surfTemp2 = NULL;
  SDL_Texture * textReturn = NULL;
  
  surfTemp = SDL_CreateRGBSurface ( 0, 10, 15, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
  SDL_LockSurface(surfTemp);
  cairo_surface_t *surface = cairo_image_surface_create_for_data ( surfTemp->pixels, CAIRO_FORMAT_ARGB32, surfTemp->w, surfTemp->h, surfTemp->pitch);
  cairo_t *cr = cairo_create (surface);
  
  cairo_move_to(cr, 0, 0);
  cairo_rel_line_to (cr, 10, 0);
  cairo_rel_line_to (cr, 0, 10);
  cairo_rel_line_to (cr, -4, 5);
  cairo_rel_line_to (cr, -2, 0);
  cairo_rel_line_to (cr, -4, -5);
  cairo_rel_line_to (cr, 0, -10);
  cairo_close_path (cr);
    
  cairo_set_line_width(cr, 2.0);
    
  cairo_set_source_rgb(cr, 0.5, 0.5, 0.5);
    
  cairo_fill_preserve(cr); // we need preserve to preserve contour lines
  cairo_set_source_rgb(cr, 0.56, 0.0, 0.0);

  cairo_stroke(cr);

  cairo_destroy (cr);
  cairo_surface_destroy (surface);
  SDL_UnlockSurface(surfTemp);

  surfTemp2 = SDL_ConvertSurfaceFormat(surfTemp, SDL_PIXELFORMAT_RGBA8888, 0);
  if((textReturn = SDL_CreateTextureFromSurface( g->renderer, surfTemp2 )) == NULL) {
    printf("Error creating texture from surface!");
  }

  SDL_FreeSurface(surfTemp);
  SDL_FreeSurface(surfTemp2);

  return textReturn;
}


SDL_Texture * CreateLivesTexture(SDLManager * g)
{
  SDL_Surface * surfTemp = NULL;
  SDL_Surface * surfTemp2 = NULL;
  SDL_Texture * textReturn = NULL;
  
  surfTemp = SDL_CreateRGBSurface ( 0, 10, 10, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
  SDL_LockSurface(surfTemp);
  cairo_surface_t *surface = cairo_image_surface_create_for_data ( surfTemp->pixels, CAIRO_FORMAT_ARGB32, surfTemp->w, surfTemp->h, surfTemp->pitch);
  cairo_t *cr = cairo_create (surface);
  
  cairo_move_to(cr, 0, 0);
  cairo_rel_line_to (cr, 0, 10);
  cairo_rel_line_to (cr, 5, -3);
  cairo_rel_line_to (cr, 5, -2);
  cairo_rel_line_to (cr, -5, -2);
  cairo_rel_line_to (cr, -5, -3);
  cairo_close_path (cr);
    
  cairo_set_line_width(cr, 1.0);
    
  cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
    
  cairo_fill(cr);

  cairo_stroke(cr);

  cairo_destroy (cr);
  cairo_surface_destroy (surface);
  SDL_UnlockSurface(surfTemp);

  surfTemp2 = SDL_ConvertSurfaceFormat(surfTemp, SDL_PIXELFORMAT_RGBA8888, 0);
  if((textReturn = SDL_CreateTextureFromSurface( g->renderer, surfTemp2 )) == NULL) {
    printf("Error creating texture from surface!");
  }

  SDL_FreeSurface(surfTemp);
  SDL_FreeSurface(surfTemp2);

  return textReturn;
}


SDL_Texture * CreateCrateTexture(SDLManager * g)
{
  SDL_Surface * surfTemp = NULL;
  SDL_Surface * surfTemp2 = NULL;
  SDL_Texture * textReturn = NULL;
  
  surfTemp = SDL_CreateRGBSurface ( 0, 45, 15, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
  SDL_LockSurface(surfTemp);
  cairo_surface_t *surface = cairo_image_surface_create_for_data ( surfTemp->pixels, CAIRO_FORMAT_ARGB32, surfTemp->w, surfTemp->h, surfTemp->pitch);
  cairo_t *cr = cairo_create (surface);
  

  
  cairo_set_line_width(cr, 2.0);
  cairo_set_source_rgb(cr, 1.0, 0.0, 1.0);
  cairo_move_to(cr, 0, 1);
  cairo_rel_line_to (cr, 45, 0);
  cairo_move_to(cr, 0, 14);
  cairo_rel_line_to (cr, 45,  0);
  cairo_stroke(cr);

  cairo_rectangle(cr, 2, 2, 11, 11);
  cairo_rectangle(cr, 17, 2, 11, 11);
  cairo_rectangle(cr, 32, 2, 11, 11);

 
  cairo_set_line_width(cr, 2.0);
    
  cairo_set_source_rgb(cr, 1.0, 0.0, 1.0);
    
  cairo_fill(cr);

  cairo_stroke(cr);

  cairo_set_source_rgb(cr, 1.0, 0.0, 0.0);

  cairo_rectangle(cr, 7, 3, 2, 2);
  cairo_rectangle(cr, 7, 9, 2, 2);

  cairo_rectangle(cr, 18, 3, 2, 2);
  cairo_rectangle(cr, 25, 3, 2, 2);
  cairo_rectangle(cr, 22, 9, 2, 2);

  cairo_rectangle(cr, 33, 7, 9, 1);
    
  cairo_stroke(cr);
  cairo_set_source_rgb(cr, 0.0, 1.0, 0.0);
    
  cairo_destroy (cr);
  cairo_surface_destroy (surface);
  SDL_UnlockSurface(surfTemp);

  surfTemp2 = SDL_ConvertSurfaceFormat(surfTemp, SDL_PIXELFORMAT_RGBA8888, 0);
  if((textReturn = SDL_CreateTextureFromSurface( g->renderer, surfTemp2 )) == NULL) {
    printf("Error creating texture from surface!");
  }

  SDL_FreeSurface(surfTemp);
  SDL_FreeSurface(surfTemp2);

  return textReturn;
}




SDL_Texture * CreateRocketTexture(SDLManager * g)
{
  SDL_Surface * surfTemp = NULL;
  SDL_Surface * surfTemp2 = NULL;
  SDL_Texture * textReturn = NULL;
  
  surfTemp = SDL_CreateRGBSurface ( 0, 30, 15, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
  SDL_LockSurface(surfTemp);
  cairo_surface_t *surface = cairo_image_surface_create_for_data ( surfTemp->pixels, CAIRO_FORMAT_ARGB32, surfTemp->w, surfTemp->h, surfTemp->pitch);
  cairo_t *cr = cairo_create (surface);
  
  cairo_move_to(cr, 2, 8);
  cairo_rel_line_to (cr, 10, -5);
  cairo_rel_line_to (cr, 0, 10);
  cairo_rel_line_to (cr, -10, -5);
  cairo_set_source_rgb(cr, 1.0, 1.0, 0.0);
  cairo_fill(cr);
    
  cairo_move_to(cr, 12, 3);
  cairo_rel_line_to (cr, 3, 3);
  cairo_rel_line_to (cr, 11, 0);
  cairo_rel_line_to (cr, 4, 2);
  cairo_rel_line_to (cr, 0, 1);
  cairo_rel_line_to (cr, -4, 2);
  cairo_rel_line_to (cr, -11, 0);
  cairo_rel_line_to (cr, -3, 3);
  cairo_close_path (cr);
    
  cairo_set_line_width(cr, 1.0);
    
  cairo_set_source_rgb(cr, 0.5, 0.5, 0.5);
  cairo_fill(cr);

  cairo_stroke(cr);

  cairo_destroy (cr);
  cairo_surface_destroy (surface);
  SDL_UnlockSurface(surfTemp);

  surfTemp2 = SDL_ConvertSurfaceFormat(surfTemp, SDL_PIXELFORMAT_RGBA8888, 0);
  if((textReturn = SDL_CreateTextureFromSurface( g->renderer, surfTemp2 )) == NULL) {
    printf("Error creating texture from surface!");
  }

  SDL_FreeSurface(surfTemp);
  SDL_FreeSurface(surfTemp2);

  return textReturn;
}




SDL_Texture * CreateMissileTexture(SDLManager * g)
{
  SDL_Surface * surfTemp = NULL;
  SDL_Surface * surfTemp2 = NULL;
  SDL_Texture * textReturn = NULL;
  
  surfTemp = SDL_CreateRGBSurface ( 0, 11, 8, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
  SDL_LockSurface(surfTemp);
  cairo_surface_t *surface = cairo_image_surface_create_for_data ( surfTemp->pixels, CAIRO_FORMAT_ARGB32, surfTemp->w, surfTemp->h, surfTemp->pitch);
  cairo_t *cr = cairo_create (surface);
  
  cairo_move_to(cr, 0, 3);
  cairo_rel_line_to (cr, 3, 0);
  cairo_rel_line_to (cr, 3, -3);
  cairo_rel_line_to (cr, 1, 0);
  cairo_rel_line_to (cr, 2, 2);
  cairo_rel_line_to (cr, 0, 3);
  cairo_rel_line_to (cr, -2, 2);
  cairo_rel_line_to (cr, -1, 0);
  cairo_rel_line_to (cr, -3, -3);
  cairo_rel_line_to (cr, -3, 0);

  cairo_close_path (cr);
    
  cairo_set_line_width(cr, 2.0);
    
  cairo_set_source_rgb(cr, 1.0, 0.0, 0.0);
    
  cairo_fill(cr);

  cairo_stroke(cr);

  cairo_destroy (cr);
  cairo_surface_destroy (surface);
  SDL_UnlockSurface(surfTemp);

  surfTemp2 = SDL_ConvertSurfaceFormat(surfTemp, SDL_PIXELFORMAT_RGBA8888, 0);
  if((textReturn = SDL_CreateTextureFromSurface( g->renderer, surfTemp2 )) == NULL) {
    printf("Error creating texture from surface!");
  }

  SDL_FreeSurface(surfTemp);
  SDL_FreeSurface(surfTemp2);

  return textReturn;
}





SDL_Texture * CreateTopBarTexture(SDLManager * g)
{
  SDL_Surface * surfTemp = NULL;
  SDL_Surface * surfTemp2 = NULL;
  SDL_Texture * textReturn = NULL;
  
  surfTemp = SDL_CreateRGBSurface ( 0, 1440, 30, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
  SDL_LockSurface(surfTemp);
  cairo_surface_t *surface = cairo_image_surface_create_for_data ( surfTemp->pixels, CAIRO_FORMAT_ARGB32, surfTemp->w, surfTemp->h, surfTemp->pitch);
  cairo_t *cr = cairo_create (surface);
  

  cairo_rectangle(cr, 0, 0, 1440, 30);
  cairo_close_path (cr);
    
  cairo_set_line_width(cr, 2.0);
    
  cairo_set_source_rgb(cr, 0.4, 0.4, 0.4);
    
  cairo_fill(cr);

  cairo_stroke(cr);

  cairo_destroy (cr);
  cairo_surface_destroy (surface);
  SDL_UnlockSurface(surfTemp);

  surfTemp2 = SDL_ConvertSurfaceFormat(surfTemp, SDL_PIXELFORMAT_RGBA8888, 0);
  if((textReturn = SDL_CreateTextureFromSurface( g->renderer, surfTemp2 )) == NULL) {
    printf("Error creating texture from surface!");
  }

  SDL_FreeSurface(surfTemp);
  SDL_FreeSurface(surfTemp2);

  return textReturn;
}


SDL_Texture * CreateBoss1Texture(SDLManager * g)
{
  SDL_Surface * surfTemp = NULL;
  SDL_Surface * surfTemp2 = NULL;
  SDL_Texture * textReturn = NULL;
  
  surfTemp = SDL_CreateRGBSurface ( 0, 211, 121, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
  SDL_LockSurface(surfTemp);
  cairo_surface_t *surface = cairo_image_surface_create_for_data ( surfTemp->pixels, CAIRO_FORMAT_ARGB32, surfTemp->w, surfTemp->h, surfTemp->pitch);
  cairo_t *cr = cairo_create (surface);
  
  cairo_move_to(cr, 5, 68);
  cairo_line_to (cr, 45, 32);
  cairo_line_to (cr, 32, 42);
  cairo_line_to (cr, 60, 34);
  cairo_line_to (cr, 96, 30);
  cairo_line_to (cr, 120, 18);
  cairo_line_to (cr, 146, 12);
  cairo_line_to (cr, 198, 12);
  cairo_line_to (cr, 145, 50);
  cairo_line_to (cr, 136, 90);
  cairo_line_to (cr, 80, 90);
  cairo_line_to (cr, 70, 100);
  cairo_line_to (cr, 35, 105);
  cairo_line_to (cr, 40, 97);
  cairo_line_to (cr, 24, 97);
  cairo_line_to (cr, 5, 88);
  cairo_close_path (cr);
    
  cairo_set_line_width(cr, 1.0);
    
  cairo_set_source_rgb(cr, 1.0, 1.0, 0.0);
    
  cairo_fill_preserve(cr);
    
  cairo_set_source_rgb(cr, 1.0, 0.0, 0.0);

  cairo_stroke(cr);

  //engine fire
  cairo_move_to(cr, 152, 48);
  cairo_line_to (cr, 188, 62);
  cairo_line_to (cr, 158, 62);
  cairo_line_to (cr, 150, 70);
  cairo_line_to (cr, 158, 75);
  cairo_line_to (cr, 182, 84);
  cairo_line_to (cr, 150, 95);
  cairo_line_to (cr, 135, 90);
  cairo_line_to (cr, 145, 53);
  cairo_set_source_rgb(cr, 0.0, 0.0, 1.0);
    
  cairo_fill(cr);

  cairo_stroke(cr);
    
  cairo_destroy (cr);
  cairo_surface_destroy (surface);
  SDL_UnlockSurface(surfTemp);

  surfTemp2 = SDL_ConvertSurfaceFormat(surfTemp, SDL_PIXELFORMAT_RGBA8888, 0);
  if((textReturn = SDL_CreateTextureFromSurface( g->renderer, surfTemp2 )) == NULL) {
    printf("Error creating texture from surface!");
  }

  SDL_FreeSurface(surfTemp);
  SDL_FreeSurface(surfTemp2);

  return textReturn;
}

//****************************************************************

SDL_Texture * CreateExplosionTexture(SDLManager * g)
{
  SDL_Surface * surfTemp = NULL;
  SDL_Surface * surfTemp2 = NULL;
  SDL_Texture * textReturn = NULL;
  
  surfTemp = SDL_CreateRGBSurface ( 0, 320, 320, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
  SDL_LockSurface(surfTemp);
  cairo_surface_t *surface = cairo_image_surface_create_for_data ( surfTemp->pixels, CAIRO_FORMAT_ARGB32, surfTemp->w, surfTemp->h, surfTemp->pitch);
  cairo_t *cr = cairo_create (surface);
  
  // first frame
  // orange blast
  cairo_move_to(cr, 15, 32);
  cairo_curve_to (cr, 15, 32, 21, 24, 27, 17);
  cairo_curve_to (cr, 27, 17, 35, 19, 46, 23);
  cairo_curve_to (cr, 46, 23, 44, 34, 37, 45);
  cairo_curve_to (cr, 37, 45, 28, 50, 17, 45);
  cairo_curve_to (cr, 17, 45, 18, 40, 15, 32);
  cairo_close_path (cr);
  cairo_set_source_rgb(cr, 1.0, 1.0, 0.0);
  cairo_fill(cr);
  //red blast
  cairo_arc (cr, 30.0, 36.0, 4, 0, 2 * M_PI);
  cairo_close_path (cr);
  cairo_set_source_rgb(cr, 1.0, 0.5, 0.0);
  cairo_fill(cr);
  // black line(s)
  cairo_curve_to (cr, 20, 37, 21, 24, 29, 19);
  cairo_curve_to (cr, 29, 19, 38, 20, 43, 33);
  cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
  cairo_set_line_width(cr, 2.0);
  cairo_stroke(cr);

  // second frame
  // orange blast
  cairo_move_to(cr, 84, 24);
  cairo_curve_to (cr, 84, 24, 98, 15, 112, 19);
  cairo_curve_to (cr, 112, 19, 119, 26, 122, 35);
  cairo_curve_to (cr, 122, 35, 116, 42, 104, 55);
  cairo_curve_to (cr, 104, 55, 86, 51, 76, 42);
  cairo_curve_to (cr, 76, 42, 75, 31, 84, 24);
  cairo_close_path (cr);
  cairo_set_source_rgb(cr, 1.0, 1.0, 0.0);
  cairo_fill(cr);
  //red blast
  cairo_arc (cr, 90.0, 36.0, 8, 0, 2 * M_PI);
  cairo_close_path (cr);
  cairo_set_source_rgb(cr, 1.0, 0.5, 0.0);
  cairo_fill(cr);
  // black line(s)
  cairo_curve_to (cr, 113, 37, 108, 38, 107, 49);
  cairo_curve_to (cr, 107, 49, 102, 55, 85, 50);
  cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
  cairo_set_line_width(cr, 2.0);
  cairo_stroke(cr);
  
  // third frame  
  // orange blast
  cairo_curve_to (cr, 142, 36, 136, 29, 144, 21);
  cairo_curve_to (cr, 144, 21, 150,  8, 162,  7);
  cairo_curve_to (cr, 162,  7, 176, 10, 178, 22);
  cairo_curve_to (cr, 142, 36, 136, 29, 144, 21);
  cairo_curve_to (cr, 144, 21, 185, 33, 176, 42);
  cairo_curve_to (cr, 176, 42, 165, 51, 148, 50);
  cairo_curve_to (cr, 148, 50, 144, 48, 142, 36);
  cairo_close_path (cr);
  cairo_set_source_rgb(cr, 1.0, 1.0, 0.0);
  cairo_fill(cr);
  //red blast
  cairo_curve_to (cr, 151, 34, 152, 26, 158, 19);
  cairo_curve_to (cr, 158, 19, 165, 18, 169, 27);
  cairo_curve_to (cr, 169, 27, 163, 31, 163, 41);
  cairo_curve_to (cr, 163, 41, 156, 47, 146, 46);
  cairo_curve_to (cr, 169, 27, 163, 31, 163, 41);
  cairo_curve_to (cr, 163, 41, 142, 42, 151, 34);
  cairo_close_path (cr);
  cairo_set_source_rgb(cr, 1.0, 0.5, 0.0);
  cairo_fill(cr);
  // black line(s)
  cairo_curve_to (cr, 164, 12, 171, 19, 177, 39);
  cairo_curve_to (cr, 177, 39, 169, 42, 157, 47);
  cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
  cairo_set_line_width(cr, 2.0);
  cairo_stroke(cr);

  // fourth frame 
  // orange blast
  cairo_curve_to (cr, 202, 55, 201, 43, 200, 24);
  cairo_curve_to (cr, 200, 24, 206, 17, 203,  8);
  cairo_curve_to (cr, 203,  8, 212,  7, 224, 12);
  cairo_curve_to (cr, 224, 12, 237,  6, 250, 23);
  cairo_curve_to (cr, 250, 23, 247, 41, 233, 57);
  cairo_curve_to (cr, 233, 57, 222, 56, 202, 55);
  cairo_close_path (cr);
  cairo_set_source_rgb(cr, 1.0, 1.0, 0.0);
  cairo_fill(cr);
  //red blast
  cairo_curve_to (cr, 227, 43, 213, 33, 218, 20);
  cairo_curve_to (cr, 218, 20, 230, 22, 236, 40);
  cairo_curve_to (cr, 236, 40, 232, 43, 227, 43);
  cairo_close_path (cr);
  cairo_set_source_rgb(cr, 1.0, 0.5, 0.0);
  cairo_fill(cr);
  // black line(s)
  cairo_curve_to (cr, 246, 21, 231, 34, 231, 52);
  cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
  cairo_set_line_width(cr, 2.0);
  cairo_stroke(cr);

  // fifth frame 
  // orange blast
  cairo_curve_to (cr, 260, 46, 267, 38, 267, 21);
  cairo_curve_to (cr, 267, 21, 271, 11, 279,  7);
  cairo_curve_to (cr, 279,  7, 287,  7, 293,  2);
  cairo_curve_to (cr, 293,  2, 307, 11, 313, 22);
  cairo_curve_to (cr, 313, 22, 319, 31, 313, 41);
  cairo_curve_to (cr, 313, 41, 307, 50, 311, 57);
  cairo_curve_to (cr, 311, 57, 293, 59, 279, 54);
  cairo_curve_to (cr, 279, 54, 274, 49, 260, 46);
  cairo_close_path (cr);
  cairo_set_source_rgb(cr, 1.0, 1.0, 0.0);
  cairo_fill(cr);
  //red blast
  cairo_curve_to (cr, 277, 43, 263, 33, 270, 20);
  cairo_curve_to (cr, 270, 20, 280, 32, 286, 45);
  cairo_curve_to (cr, 286, 45, 284, 43, 277, 43);
  cairo_close_path (cr);
  cairo_set_source_rgb(cr, 1.0, 0.5, 0.0);
  cairo_fill(cr);
  // black line(s)
  cairo_curve_to (cr, 281, 19, 268, 34, 267, 49);
  cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
  cairo_set_line_width(cr, 2.0);
  cairo_stroke(cr);

  // sixth frame 
  // orange blast
  cairo_curve_to (cr, 13, 107,  12, 88,  19, 76);
  cairo_curve_to (cr,  19, 76,  30, 73,  40, 74);
  cairo_curve_to (cr,  40, 74,  46, 72,  55, 80);
  cairo_curve_to (cr,  55, 80,  62, 88,  59, 103);
  cairo_curve_to (cr,  59, 103, 52, 115, 40, 124);
  cairo_curve_to (cr,  40, 124, 26, 118, 13, 107);
  cairo_close_path (cr);
  cairo_set_source_rgb(cr, 1.0, 1.0, 0.0);
  cairo_fill(cr);
  //red blast
  cairo_set_source_rgb(cr, 1.0, 0.5, 0.0);
  cairo_arc (cr, 45.0, 86.0, 2, 0, 2 * M_PI);
  cairo_fill(cr);
  cairo_arc (cr, 43.0, 86.0, 2, 0, 2 * M_PI);
  cairo_fill(cr);
  cairo_arc (cr, 40.0, 86.0, 2, 0, 2 * M_PI);
  cairo_fill(cr);
  cairo_arc (cr, 46.0, 102.0, 4, 0, 2 * M_PI);
  cairo_fill(cr);
  cairo_arc (cr, 24.0, 104.0, 4, 0, 2 * M_PI);
  cairo_fill(cr);
  // black line(s)
  cairo_curve_to (cr, 14, 81, 12, 94, 24, 112);
  cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
  cairo_set_line_width(cr, 2.0);
  cairo_stroke(cr);

  // seventh frame 
  // orange blast
  cairo_curve_to (cr, 74, 121,  70, 106, 76, 96);
  cairo_curve_to (cr,  76, 96,  75, 82,  85, 74);
  cairo_curve_to (cr,  85, 74,  99, 69,  116, 75);
  cairo_curve_to (cr,  116, 75, 128, 86, 128, 99);
  cairo_curve_to (cr,  128, 99, 117, 106, 121, 118);
  cairo_curve_to (cr,  121, 118, 109, 127, 96, 122);
  cairo_curve_to (cr,  96, 122, 87, 126, 74, 121);
  cairo_close_path (cr);
  cairo_set_source_rgb(cr, 1.0, 1.0, 0.0);
  cairo_fill(cr);
  //red blast
  cairo_set_source_rgb(cr, 1.0, 0.5, 0.0);
  cairo_curve_to (cr,  105, 111, 91, 101, 95, 91);
  cairo_curve_to (cr,   95, 91, 107, 95, 105, 111);
  cairo_fill(cr);
  // black line(s)
  cairo_curve_to (cr, 119, 86, 124, 101, 113, 120);
  cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
  cairo_set_line_width(cr, 2.0);
  cairo_stroke(cr);

  // eighth frame 
  // orange blast
  cairo_curve_to (cr, 137, 116, 142, 97, 143, 78);
  cairo_curve_to (cr, 143, 78, 158, 74, 177, 73);
  cairo_curve_to (cr, 177, 73, 187, 81, 186, 97);
  cairo_curve_to (cr, 186, 97, 177, 105, 174, 119);
  cairo_curve_to (cr, 174, 119, 156, 123, 137, 116);
  cairo_close_path (cr);
  cairo_set_source_rgb(cr, 1.0, 1.0, 0.0);
  cairo_fill(cr);
  //red blast
  cairo_set_source_rgb(cr, 1.0, 0.5, 0.0);
  cairo_curve_to (cr,  156, 92, 149, 85, 157, 79);
  cairo_curve_to (cr,  157, 79, 162, 87, 156, 92);
  cairo_fill(cr);
  // black line(s)
  cairo_curve_to (cr, 140, 92, 138, 114, 171, 121);
  cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
  cairo_set_line_width(cr, 2.0);
  cairo_stroke(cr);

  // ninth frame 
  // orange blast
  cairo_curve_to (cr, 196, 114, 197, 101, 205, 88);
  cairo_curve_to (cr, 205, 88, 209, 70, 229, 69);
  cairo_curve_to (cr, 229, 97, 244, 80, 249, 120);
  cairo_curve_to (cr, 249, 120, 234, 128, 196, 114);
  cairo_close_path (cr);
  cairo_set_source_rgb(cr, 1.0, 1.0, 0.0);
  cairo_fill(cr);
  //red blast
  cairo_set_source_rgb(cr, 1.0, 0.5, 0.0);
  cairo_arc (cr, 22.0, 89.0, 4, 0, 2 * M_PI);
  cairo_fill(cr);
  // black line(s)
  cairo_curve_to (cr, 234, 70, 247, 101, 242, 121);
  cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
  cairo_set_line_width(cr, 2.0);
  cairo_stroke(cr);

  // 10th frame 
  // orange blast
  cairo_curve_to (cr, 256, 118, 256, 108, 268, 100);
  cairo_curve_to (cr, 268, 100, 263, 93, 265, 72);
  cairo_curve_to (cr, 265, 72, 276, 67, 301, 67);
  cairo_curve_to (cr, 301, 67, 318, 80, 313, 107);
  cairo_curve_to (cr, 313, 107, 304, 117, 291, 122);
  cairo_curve_to (cr, 291, 122, 276, 123, 256, 118);
  cairo_close_path (cr);
  cairo_set_source_rgb(cr, 1.0, 1.0, 0.0);
  cairo_fill(cr);
  //red blast
  cairo_set_source_rgb(cr, 1.0, 0.5, 0.0);
  cairo_curve_to (cr, 294, 113, 284, 103, 290, 89);
  cairo_curve_to (cr, 290, 89, 297, 100, 294, 113);
  cairo_fill(cr);
  // black line(s)
  cairo_curve_to (cr, 273, 76, 300, 76, 314, 92);
  cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
  cairo_set_line_width(cr, 2.0);
  cairo_stroke(cr);

  // 11th frame 
  // orange blast
  cairo_curve_to (cr, 5, 179, 5, 164, 17, 152);
  cairo_curve_to (cr, 17, 152, 17, 140, 48, 138);
  cairo_curve_to (cr, 48, 138, 60, 152, 51, 172);
  cairo_curve_to (cr, 51, 172, 49, 189, 34, 186);
  cairo_curve_to (cr, 34, 186, 24, 179,  5, 179);
  cairo_close_path (cr);
  cairo_set_source_rgb(cr, 1.0, 1.0, 0.0);
  cairo_fill(cr);
  //red blast
  cairo_set_source_rgb(cr, 1.0, 0.5, 0.0);
  cairo_curve_to (cr, 35, 177, 25, 173, 18, 164);
  cairo_curve_to (cr, 18, 164, 24, 154, 41, 152);
  cairo_curve_to (cr, 41, 152, 42, 160, 35, 177);
  cairo_fill(cr);
  // black line(s)
  cairo_curve_to (cr, 50, 183, 50, 167, 60, 154);
  cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
  cairo_set_line_width(cr, 2.0);
  cairo_stroke(cr);

  // 12th frame 
  // orange blast
  cairo_curve_to (cr, 67, 174, 69, 162, 78, 156);
  cairo_curve_to (cr, 78, 156, 84, 140, 99, 134);
  cairo_curve_to (cr, 99, 134, 117, 143, 124, 160);
  cairo_curve_to (cr, 124, 160, 103, 181, 100, 183);
  cairo_curve_to (cr, 100, 183, 85, 186, 67, 174);
  cairo_close_path (cr);
  cairo_set_source_rgb(cr, 1.0, 1.0, 0.0);
  cairo_fill(cr);
  //red blast
  cairo_set_source_rgb(cr, 1.0, 0.5, 0.0);
  cairo_curve_to (cr, 101, 164, 92, 155, 100, 150);
  cairo_curve_to (cr, 100, 150, 109, 158, 101, 164);
  cairo_fill(cr);
  // black line(s)
  cairo_curve_to (cr, 121, 160, 111, 138, 92, 135);
  cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
  cairo_set_line_width(cr, 2.0);
  cairo_stroke(cr);

  // 13th frame 
  // orange blast
  cairo_curve_to (cr, 131, 177, 128, 162, 140, 156);
  cairo_curve_to (cr, 140, 156, 143, 147, 142, 134);
  cairo_curve_to (cr, 142, 134, 164, 130, 178, 134);
  cairo_curve_to (cr, 178, 134, 187, 147, 190, 163);
  cairo_curve_to (cr, 190, 163, 160, 187, 131, 177);
  cairo_close_path (cr);
  cairo_set_source_rgb(cr, 1.0, 1.0, 0.0);
  cairo_fill(cr);
  //red blast
  cairo_set_source_rgb(cr, 1.0, 0.5, 0.0);
  cairo_curve_to (cr, 164, 173, 159, 154, 168, 146);
  cairo_curve_to (cr, 168, 146, 177, 161, 164, 173);
  cairo_fill(cr);
  // black line(s)
  cairo_curve_to (cr, 185, 142, 190, 164, 161, 188);
  cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
  cairo_set_line_width(cr, 2.0);
  cairo_stroke(cr);

  // 14th frame 
  // orange blast
  cairo_curve_to (cr, 194, 163, 204, 156, 210, 137);
  cairo_curve_to (cr, 210, 137, 237, 137, 253, 155);
  cairo_curve_to (cr, 253, 155, 248, 170, 230, 191);
  cairo_curve_to (cr, 230, 191, 209, 185, 194, 163);
  cairo_close_path (cr);
  cairo_set_source_rgb(cr, 1.0, 1.0, 0.0);
  cairo_fill(cr);
  //red blast
  cairo_set_source_rgb(cr, 1.0, 0.5, 0.0);
  cairo_curve_to (cr, 223, 180, 217, 172, 221, 159);
  cairo_curve_to (cr, 221, 159, 215, 151, 238, 147);
  cairo_curve_to (cr, 238, 147, 247, 158, 243, 173);
  cairo_curve_to (cr, 243, 173, 229, 178, 223, 180);
  cairo_fill(cr);
  // black line(s)
  cairo_curve_to (cr, 202, 147, 211, 137, 238, 137);
  cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
  cairo_set_line_width(cr, 2.0);
  cairo_stroke(cr);

  // 15th frame 
  // orange blast
  cairo_curve_to (cr, 261, 177, 261, 162, 271, 147);
  cairo_curve_to (cr, 271, 147, 274, 137, 300, 133);
  cairo_curve_to (cr, 300, 133, 317, 150, 314, 178);
  cairo_curve_to (cr, 314, 178, 285, 192, 261, 177);
  cairo_close_path (cr);
  cairo_set_source_rgb(cr, 1.0, 1.0, 0.0);
  cairo_fill(cr);
  //red blast
  cairo_set_source_rgb(cr, 1.0, 0.5, 0.0);
  cairo_curve_to (cr, 295, 181, 265, 175, 273, 156);
  cairo_curve_to (cr, 273, 156, 284, 140, 299, 136);
  cairo_curve_to (cr, 299, 136, 308, 157, 295, 181);
  cairo_fill(cr);
  // black line(s)
  cairo_curve_to (cr, 283, 188, 306, 176, 314, 146);
  cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
  cairo_set_line_width(cr, 2.0);
  cairo_stroke(cr);

  // 16th frame 
  // orange blast
  // not anymore
  //red blast
  cairo_set_source_rgb(cr, 1.0, 0.5, 0.0);
  cairo_curve_to (cr, 5, 240, 15, 219, 7, 203);
  cairo_curve_to (cr, 7, 203, 31, 195, 62, 211);
  cairo_curve_to (cr, 62, 211, 60, 235, 45, 255);
  cairo_curve_to (cr, 45, 255, 24, 252, 5, 240);
  cairo_fill(cr);
  // black line(s)
  cairo_curve_to (cr, 50, 200, 63, 211, 59, 238);
  cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
  cairo_set_line_width(cr, 2.0);
  cairo_stroke(cr);

  // 17th frame 
  // orange blast
  // not anymore
  //red blast
  cairo_set_source_rgb(cr, 1.0, 0.5, 0.0);
  cairo_curve_to (cr, 70, 244, 72, 225, 90, 214);
  cairo_curve_to (cr, 90, 214, 92, 200, 119, 204);
  cairo_curve_to (cr, 119, 204, 118, 241, 96, 257);
  cairo_curve_to (cr, 96, 257, 78, 251, 70, 244);
  cairo_fill(cr);
  // black line(s)
  cairo_curve_to (cr, 86, 201, 114, 208, 119, 227);
  cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
  cairo_set_line_width(cr, 2.0);
  cairo_stroke(cr);

  // 18th frame 
  // orange blast
  // not anymore
  //red blast
  cairo_set_source_rgb(cr, 1.0, 0.5, 0.0);
  cairo_curve_to (cr, 144, 252, 135, 235, 152, 201);
  cairo_curve_to (cr, 152, 201, 176, 201, 184, 233);
  cairo_curve_to (cr, 184, 233, 158, 254, 144, 252);
  cairo_fill(cr);
  // black line(s)
  cairo_curve_to (cr, 143, 206, 175, 203, 185, 234);
  cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
  cairo_set_line_width(cr, 2.0);
  cairo_stroke(cr);

  // 19th frame 
  // orange blast
  // not anymore
  //red blast
  cairo_set_source_rgb(cr, 1.0, 0.5, 0.0);
  cairo_curve_to (cr, 200, 242, 208, 220, 216, 196);
  cairo_curve_to (cr, 216, 196, 231, 197, 249, 219);
  cairo_curve_to (cr, 249, 219, 235, 251, 200, 242);
  cairo_fill(cr);
  // black line(s)
  cairo_curve_to (cr, 202, 237, 210, 222, 210, 207);
  cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
  cairo_set_line_width(cr, 2.0);
  cairo_stroke(cr);

  // 20th frame 
  // orange blast
  // not anymore
  //red blast
  cairo_set_source_rgb(cr, 1.0, 0.5, 0.0);
  cairo_curve_to (cr, 262, 238, 281, 203, 302, 207);
  cairo_curve_to (cr, 302, 207, 306, 222, 316, 238);
  cairo_curve_to (cr, 316, 238, 300, 253, 279, 254);
  cairo_curve_to (cr, 279, 254, 267, 247, 262, 238);
  cairo_fill(cr);
  // black line(s)
  cairo_curve_to (cr, 315, 231, 298, 220, 289, 202);
  cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
  cairo_set_line_width(cr, 2.0);
  cairo_stroke(cr);

  // 21th frame 
  // orange blast
  // not anymore
  //red blast
  cairo_set_source_rgb(cr, 1.0, 0.5, 0.0);
  cairo_curve_to (cr, 12, 313, 9, 293, 26, 264);
  cairo_curve_to (cr, 26, 264, 42, 264, 50, 283);
  cairo_curve_to (cr, 50, 283, 53, 300, 12, 313);
  cairo_fill(cr);
  // black line(s)
  cairo_curve_to (cr, 40, 265, 48, 279, 48, 292);
  cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
  cairo_set_line_width(cr, 2.0);
  cairo_stroke(cr);

  // 22th frame 
  // orange blast
  // not anymore
  //red blast
  cairo_set_source_rgb(cr, 1.0, 0.5, 0.0);
  cairo_curve_to (cr, 99, 309, 85, 296, 85, 278);
  cairo_curve_to (cr, 85, 278, 104, 272, 110, 287);
  cairo_curve_to (cr, 110, 287, 106, 301, 99, 309);
  cairo_fill(cr);
  // black line(s)
  cairo_curve_to (cr, 86, 279, 104, 274, 109, 293);
  cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
  cairo_set_line_width(cr, 2.0);
  cairo_stroke(cr);

  // 23th frame 
  // orange blast
  // not anymore
  //red blast
  cairo_set_source_rgb(cr, 1.0, 0.5, 0.0);
  cairo_curve_to (cr, 164, 299, 148, 289, 152, 277);
  cairo_curve_to (cr, 152, 277, 169, 285, 164, 299);  
  cairo_fill(cr);
  // black line(s)
  cairo_curve_to (cr, 163, 280, 163, 294, 152, 294);
  cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
  cairo_set_line_width(cr, 2.0);
  cairo_stroke(cr);

  
  cairo_destroy (cr);
  cairo_surface_destroy (surface);
  SDL_UnlockSurface(surfTemp);

  surfTemp2 = SDL_ConvertSurfaceFormat(surfTemp, SDL_PIXELFORMAT_RGBA8888, 0);
  if((textReturn = SDL_CreateTextureFromSurface( g->renderer, surfTemp2 )) == NULL) {
    printf("Error creating texture from surface!");
  }

  SDL_FreeSurface(surfTemp);
  SDL_FreeSurface(surfTemp2);

  return textReturn;
}




SDL_Texture * CreateFiremineTexture(SDLManager * g)
{
  SDL_Surface * surfTemp = NULL;
  SDL_Surface * surfTemp2 = NULL;
  SDL_Texture * textReturn = NULL;
  
  surfTemp = SDL_CreateRGBSurface ( 0, 60, 30, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
  SDL_LockSurface(surfTemp);
  cairo_surface_t *surface = cairo_image_surface_create_for_data ( surfTemp->pixels, CAIRO_FORMAT_ARGB32, surfTemp->w, surfTemp->h, surfTemp->pitch);
  cairo_t *cr = cairo_create (surface);
  
  cairo_move_to(cr, 0, 0);
  cairo_line_to (cr, 0, 5);
  cairo_line_to (cr, 17, 0);
  cairo_line_to (cr, 30, 14);
  cairo_line_to (cr, 17, 30);
  cairo_line_to (cr, 0, 24);
  cairo_close_path (cr);
    
  cairo_set_line_width(cr, 1.0);
    
  cairo_set_source_rgb(cr, 0.0, 1.0, 0.0);
    
  cairo_fill(cr);

  cairo_line_to (cr, 30, 5);
  cairo_line_to (cr, 47, 0);
  cairo_line_to (cr, 60, 14);
  cairo_line_to (cr, 47, 30);
  cairo_line_to (cr, 30, 24);
  cairo_close_path (cr);    
  cairo_set_source_rgb(cr, 1.0, 0.0, 0.0);
    
  cairo_fill(cr);

  
  cairo_stroke(cr);

  cairo_destroy (cr);
  cairo_surface_destroy (surface);
  SDL_UnlockSurface(surfTemp);

  surfTemp2 = SDL_ConvertSurfaceFormat(surfTemp, SDL_PIXELFORMAT_RGBA8888, 0);
  if((textReturn = SDL_CreateTextureFromSurface( g->renderer, surfTemp2 )) == NULL) {
    printf("Error creating texture from surface!");
  }

  SDL_FreeSurface(surfTemp);
  SDL_FreeSurface(surfTemp2);

  return textReturn;
}






SDL_Texture * CreateFirewallTexture(SDLManager * g)
{
  SDL_Surface * surfTemp = NULL;
  SDL_Surface * surfTemp2 = NULL;
  SDL_Texture * textReturn = NULL;
  
  surfTemp = SDL_CreateRGBSurface ( 0, 20, 590, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
  SDL_LockSurface(surfTemp);
  cairo_surface_t *surface = cairo_image_surface_create_for_data ( surfTemp->pixels, CAIRO_FORMAT_ARGB32, surfTemp->w, surfTemp->h, surfTemp->pitch);
  cairo_t *cr = cairo_create (surface);

  cairo_rectangle(cr, 0, 0, 20, 590);
  cairo_set_line_width(cr, 1.0);
    
  cairo_set_source_rgb(cr, 1.0, 1.0, 0.0);
    
  cairo_fill(cr);
  
  cairo_stroke(cr);

  cairo_destroy (cr);
  cairo_surface_destroy (surface);
  SDL_UnlockSurface(surfTemp);

  surfTemp2 = SDL_ConvertSurfaceFormat(surfTemp, SDL_PIXELFORMAT_RGBA8888, 0);
  if((textReturn = SDL_CreateTextureFromSurface( g->renderer, surfTemp2 )) == NULL) {
    printf("Error creating texture from surface!");
  }

  SDL_FreeSurface(surfTemp);
  SDL_FreeSurface(surfTemp2);

  return textReturn;
}





SDL_Texture * CreateCannonTexture(SDLManager * g)
{
  SDL_Surface * surfTemp = NULL;
  SDL_Surface * surfTemp2 = NULL;
  SDL_Texture * textReturn = NULL;
  
  surfTemp = SDL_CreateRGBSurface ( 0, 64, 32, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
  SDL_LockSurface(surfTemp);
  cairo_surface_t *surface = cairo_image_surface_create_for_data ( surfTemp->pixels, CAIRO_FORMAT_ARGB32, surfTemp->w, surfTemp->h, surfTemp->pitch);
  cairo_t *cr = cairo_create (surface);

  cairo_rectangle(cr, 1, 1, 30, 30);
  cairo_set_line_width(cr, 1.0);
  cairo_set_source_rgb(cr, 0.5, 0.5, 0.5);
    
  cairo_fill(cr);
  
  cairo_stroke(cr);

  cairo_curve_to (cr, 5, 28, 5, 24, 13, 16);
  cairo_curve_to (cr, 13, 16, 17, 3, 23, 3);
  cairo_curve_to (cr, 23, 3, 30, 17, 25, 21);
  cairo_curve_to (cr, 25, 21, 18, 22, 7, 28);
  cairo_close_path (cr);    
  cairo_set_source_rgb(cr, 1, 0, 0);
  cairo_fill(cr);
  cairo_stroke(cr);
  //***
  cairo_rectangle(cr, 31, 31, 30, 30);
  cairo_set_line_width(cr, 1.0);
  cairo_set_source_rgb(cr, 0.9, 0.5, 0.2);
    
  cairo_fill(cr);
  
  cairo_stroke(cr);

  cairo_curve_to (cr, 35, 28, 35, 24, 43, 16);
  cairo_curve_to (cr, 43, 16, 47, 3, 53, 3);
  cairo_curve_to (cr, 53, 3, 60, 17, 55, 21);
  cairo_curve_to (cr, 55, 21, 48, 22, 37, 28);
  cairo_close_path (cr);    
  cairo_set_source_rgb(cr, 1, 0, 0);
  cairo_fill(cr);
  cairo_stroke(cr);

  cairo_destroy (cr);
  cairo_surface_destroy (surface);
  SDL_UnlockSurface(surfTemp);

  surfTemp2 = SDL_ConvertSurfaceFormat(surfTemp, SDL_PIXELFORMAT_RGBA8888, 0);
  if((textReturn = SDL_CreateTextureFromSurface( g->renderer, surfTemp2 )) == NULL) {
    printf("Error creating texture from surface!");
  }

  SDL_FreeSurface(surfTemp);
  SDL_FreeSurface(surfTemp2);

  return textReturn;
}




SDL_Texture * CreateSpaceTexture(SDLManager * g)
{
  SDL_Surface * surfTemp = NULL;
  SDL_Surface * surfTemp2 = NULL;
  SDL_Texture * textReturn = NULL;
  
  surfTemp = SDL_CreateRGBSurface ( 0, 1440, 480, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
  SDL_LockSurface(surfTemp);
  cairo_surface_t *surface = cairo_image_surface_create_for_data ( surfTemp->pixels, CAIRO_FORMAT_ARGB32, surfTemp->w, surfTemp->h, surfTemp->pitch);
  cairo_t *cr = cairo_create (surface);


  cairo_set_source_rgb(cr, 0, 0, 0);
  cairo_paint(cr);
  
  double scale = 0.1;
  int x, y;
  cairo_scale(cr, scale, scale);    
  for(int i = 0; i < 100; i++) {
    x = (rand()%1440) * pow(scale, -1); // x and y shall not be affected by scaling!
    y = (rand()%480) * pow(scale, -1);

    cairo_set_line_width(cr, 2.0);
    cairo_move_to(cr, x + 20, y + 0);
    cairo_rel_line_to(cr, -10, 30);
    cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
    cairo_move_to(cr, x + 0, y + 10);
    cairo_rel_line_to(cr, 30, 10);
    cairo_move_to(cr, x + 12, y + 8);
    cairo_rel_line_to(cr, 7, 17);
    cairo_move_to(cr, x + 21, y + 10);
    cairo_rel_line_to(cr, -13, 11);
    cairo_stroke(cr);
  }
  
  cairo_destroy (cr);
  cairo_surface_destroy (surface);
  SDL_UnlockSurface(surfTemp);

  surfTemp2 = SDL_ConvertSurfaceFormat(surfTemp, SDL_PIXELFORMAT_RGBA8888, 0);
  if((textReturn = SDL_CreateTextureFromSurface( g->renderer, surfTemp2 )) == NULL) {
    printf("Error creating texture from surface!");
  }

  SDL_FreeSurface(surfTemp);
  SDL_FreeSurface(surfTemp2);

  return textReturn;
}




SDL_Texture * CreateTitleTexture(SDLManager * g)
{
  SDL_Surface * surfTemp = NULL;
  SDL_Surface * surfTemp2 = NULL;
  SDL_Texture * textReturn = NULL;
  
  surfTemp = SDL_CreateRGBSurface ( 0, 260, 165, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
  SDL_LockSurface(surfTemp);
  cairo_surface_t *surface = cairo_image_surface_create_for_data ( surfTemp->pixels, CAIRO_FORMAT_ARGB32, surfTemp->w, surfTemp->h, surfTemp->pitch);
  cairo_t *cr = cairo_create (surface);

cairo_set_line_width(cr, 6.0);
  cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
  // "o"
  cairo_move_to(cr, 250, 4);
  cairo_rel_line_to(cr, 0, 30);
  cairo_rel_line_to(cr, -30, 6);
  cairo_rel_line_to(cr, 0, -30);
  cairo_rel_line_to(cr, 33, -6);
  cairo_stroke(cr);
  // "g"
  cairo_move_to(cr, 210, 12);
  cairo_rel_line_to(cr, -40, 6);  
  cairo_rel_line_to(cr, 0, 35);
  cairo_rel_line_to(cr, 35, -10);  
  cairo_rel_line_to(cr, -25, -10);
  cairo_stroke(cr);
  // "r"
  cairo_move_to(cr, 130, 75);
  cairo_rel_line_to(cr, 0, -50);
  cairo_rel_line_to(cr, 30, -6);
  cairo_rel_line_to(cr, -30, 40);
  cairo_rel_line_to(cr, 15, -20);
  cairo_rel_line_to(cr, 10, 30);
  cairo_stroke(cr);
  // "a"
  cairo_move_to(cr, 80, 85);
  cairo_rel_line_to(cr, 0, -50);
  cairo_rel_line_to(cr, 30, -6);
  cairo_rel_line_to(cr, 0, 50);
  cairo_rel_line_to(cr, 0, -20);
  cairo_rel_line_to(cr, -30, -10);
  cairo_stroke(cr);
  // "c"
  cairo_move_to(cr, 10, 45);
  cairo_rel_line_to(cr, 0, 70);
  cairo_rel_line_to(cr, 240, -60);
  cairo_move_to(cr, 7, 45);
  cairo_rel_line_to(cr, 50, -10);
  // the "z"
  cairo_move_to(cr, 10, 135);
  cairo_rel_line_to(cr, 240, -60);
  cairo_rel_line_to(cr, -240, 80);
  cairo_rel_line_to(cr, 240, -40);

  //  cairo_rel_line_to(cr, -30, 0);
  cairo_stroke(cr);
  
  
  cairo_destroy (cr);
  cairo_surface_destroy (surface);
  SDL_UnlockSurface(surfTemp);

  surfTemp2 = SDL_ConvertSurfaceFormat(surfTemp, SDL_PIXELFORMAT_RGBA8888, 0);
  if((textReturn = SDL_CreateTextureFromSurface( g->renderer, surfTemp2 )) == NULL) {
    printf("Error creating texture from surface!");
  }

  SDL_FreeSurface(surfTemp);
  SDL_FreeSurface(surfTemp2);

  return textReturn;
}




SDL_Texture * CreateTextTexture(SDLManager * g, char * textToDisplay, int textureWidth, int textureHeight)
{
  SDL_Surface * surfTemp = NULL;
  SDL_Surface * surfTemp2 = NULL;
  SDL_Texture * textReturn = NULL;
  PangoLayout* layout = NULL;
  PangoFontDescription* fontDesc = NULL;
  
  surfTemp = SDL_CreateRGBSurface ( 0, textureWidth, textureHeight, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
  SDL_LockSurface(surfTemp);
  cairo_surface_t *surface = cairo_image_surface_create_for_data ( surfTemp->pixels, CAIRO_FORMAT_ARGB32, surfTemp->w, surfTemp->h, surfTemp->pitch);
  cairo_t *cr = cairo_create (surface);

  layout = pango_cairo_create_layout(cr);

  // if we don't set font descripion things below, we will end up with some default
  fontDesc = pango_font_description_from_string("Vera");
  pango_layout_set_font_description(layout, fontDesc);
  pango_font_description_free(fontDesc);
 
  /* set the width around which pango will wrap */
  pango_layout_set_width(layout, textureWidth * PANGO_SCALE);

  pango_layout_set_alignment(layout, PANGO_ALIGN_CENTER);
  
  pango_layout_set_markup(layout, textToDisplay, -1);
  //pango_layout_set_text(layout, text, -1);
 
  pango_cairo_show_layout(cr, layout);
  
  cairo_destroy (cr);
  cairo_surface_destroy (surface);
  SDL_UnlockSurface(surfTemp);

  surfTemp2 = SDL_ConvertSurfaceFormat(surfTemp, SDL_PIXELFORMAT_RGBA8888, 0);
  if((textReturn = SDL_CreateTextureFromSurface( g->renderer, surfTemp2 )) == NULL) {
    printf("Error creating texture from surface!");
  }

  SDL_FreeSurface(surfTemp);
  SDL_FreeSurface(surfTemp2);

  return textReturn;
}





SDL_Texture * CreateOptionsBgTexture(SDLManager * g)
{
  SDL_Surface * surfTemp = NULL;
  SDL_Surface * surfTemp2 = NULL;
  SDL_Texture * textReturn = NULL;
  
  surfTemp = SDL_CreateRGBSurface ( 0, 600, 400, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
  SDL_LockSurface(surfTemp);
  cairo_surface_t *surface = cairo_image_surface_create_for_data ( surfTemp->pixels, CAIRO_FORMAT_ARGB32, surfTemp->w, surfTemp->h, surfTemp->pitch);
  cairo_t *cr = cairo_create (surface);

  // thanks cairo samples! https://www.cairographics.org/samples/
  double x         = 5,        /* parameters like cairo_rectangle */
    y         = 5,
    width         = 590,
    height        = 390,
    aspect        = 1.0,      /* aspect ratio */
    cornerRadius = height / 10.0;   /* and corner curvature radius */

  double radius = cornerRadius / aspect;
  double degrees = M_PI / 180.0;

  cairo_new_sub_path (cr);
  cairo_arc (cr, x + width - radius, y + radius, radius, -90 * degrees, 0 * degrees);
  cairo_arc (cr, x + width - radius, y + height - radius, radius, 0 * degrees, 90 * degrees);
  cairo_arc (cr, x + radius, y + height - radius, radius, 90 * degrees, 180 * degrees);
  cairo_arc (cr, x + radius, y + radius, radius, 180 * degrees, 270 * degrees);
  cairo_close_path (cr);

  cairo_set_source_rgb (cr, 0, 0, 0);
  cairo_fill_preserve (cr);
  cairo_set_source_rgba (cr, 1, 1, 1, 1);
  cairo_set_line_width (cr, 10.0);
  cairo_stroke (cr);
  cairo_destroy (cr);
  cairo_surface_destroy (surface);
  SDL_UnlockSurface(surfTemp);

  surfTemp2 = SDL_ConvertSurfaceFormat(surfTemp, SDL_PIXELFORMAT_RGBA8888, 0);
  if((textReturn = SDL_CreateTextureFromSurface( g->renderer, surfTemp2 )) == NULL) {
    printf("Error creating texture from surface!");
  }

  SDL_FreeSurface(surfTemp);
  SDL_FreeSurface(surfTemp2);

  return textReturn;
}
