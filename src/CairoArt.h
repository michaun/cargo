/*
  This file is a part of cargo - a space shooter game
  Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _CAIROART_H_
#define _CAIROART_H_

#include "SDLManager.h"

SDL_Texture * CreateCairoShipTexture(SDLManager * g);
SDL_Texture * CreateCairoEnemyTexture(SDLManager * g);
SDL_Texture * CreateShieldTexture(SDLManager * g);
SDL_Texture * CreateLivesTexture(SDLManager * g);
SDL_Texture * CreateCrateTexture(SDLManager * g);
SDL_Texture * CreateRocketTexture(SDLManager * g);
SDL_Texture * CreateMissileTexture(SDLManager * g);
SDL_Texture * CreateTopBarTexture(SDLManager * g);
SDL_Texture * CreateBoss1Texture(SDLManager * g);
SDL_Texture * CreateExplosionTexture(SDLManager * g);
SDL_Texture * CreateFiremineTexture(SDLManager * g);
SDL_Texture * CreateFirewallTexture(SDLManager * g);
SDL_Texture * CreateCannonTexture(SDLManager * g);
SDL_Texture * CreateSpaceTexture(SDLManager * g);
SDL_Texture * CreateTitleTexture(SDLManager * g);

SDL_Texture * CreateTextTexture(SDLManager * g, char * textToDisplay, int textureWidth, int textureHeight);
SDL_Texture * CreateOptionsBgTexture(SDLManager * g);

#endif
