/*
  This file is a part of cargo - a space shooter game
  Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//==============================================================================
#include "ChapterOne.h"
#include "Behaviors.h"
#include "Collisions.h"
#include "CairoArt.h"
#include "TextDB.h"

//==============================================================================

static void HandleChapterOneFrame(SDLManager * g, ChapterOne *t, MapsManager * mam, ListManager * lm)
{
  SLList * iterator = NULL;

  if(t->Paused) return; // if Paused, do not update anything on the scene.
  g->camera.x+=FLYING_SPEED;
  if(t->onIntroScreen) return; // we are reading Objectives for mission, don't do that war shit just yet. thx.
  t->totalDistance += FLYING_SPEED;
  printf("totalDistance: %d\n", t->totalDistance);
  (*mam->ReturnMap(RANDOMIZED_MAP))(mam, g, t->totalDistance);
  
  for(iterator = lm->chapterOneHeroes; iterator; iterator = iterator->next) ((Flying*)iterator->data)->fm->Handle(g, iterator);
  for(iterator = lm->chapterOneEnemies; iterator; iterator = iterator->next) ((Flying*)iterator->data)->fm->Handle(g, iterator);
  for(iterator = lm->chapterOneBosses; iterator; iterator = iterator->next) ((Flying*)iterator->data)->fm->Handle(g, iterator);
  for(iterator = lm->chapterOneExplosions; iterator; iterator = iterator->next) ((Explosion*)iterator->data)->em->Handle(g, iterator);
  for(iterator = lm->chapterOnePickables; iterator; iterator = iterator->next) ((Pickable*)iterator->data)->pm->Handle(g, iterator);
  for(iterator = lm->chapterOneMissiles; iterator; iterator = iterator->next) ((Missile*)iterator->data)->mm->Handle(g, iterator);
  for(iterator = lm->chapterOneEnemyMissiles; iterator; iterator = iterator->next) ((Missile*)iterator->data)->mm->Handle(g, iterator);
}
//==============================================================================
//==============================================================================
//==============================================================================

static void DrawHealth(SDLManager * g, ListManager *lm)
{
  Flying * hero;
  if(lm->chapterOneHeroes) {
    hero = ((Flying *) lm->chapterOneHeroes->data);

    int lifew = ((SDL_Rect *)((Resource *)GetNthElement(lm->chapterOneTextures, SHIPICON_RES)->data)->clip->data)->w;
    int lifeh = ((SDL_Rect *)((Resource *)GetNthElement(lm->chapterOneTextures, SHIPICON_RES)->data)->clip->data)->h;
    SDL_Rect livesplace;

    livesplace.y = 5;
    livesplace.w = lifew;
    livesplace.h = lifeh;
    for(int i = 0; i < hero->lives; i++) {
      livesplace.x = 5 + (i * (lifew + 5));
      SDL_RenderCopy(g->renderer, ((Resource *)GetNthElement(lm->chapterOneTextures, SHIPICON_RES)->data)->texture, NULL, &livesplace);
    }

    int shieldw = ((SDL_Rect *)((Resource *)GetNthElement(lm->chapterOneTextures, SHIELD_RES)->data)->clip->data)->w;
    int shieldh = ((SDL_Rect *)((Resource *)GetNthElement(lm->chapterOneTextures, SHIELD_RES)->data)->clip->data)->h;
    SDL_Rect shieldplace;
    shieldplace.y = livesplace.y + livesplace.h;
    shieldplace.w = shieldw;
    shieldplace.h = shieldh;
    for(int i = 0; i < hero->health; i++) {
      shieldplace.x = 5 + (i * (shieldw + 5));
      SDL_RenderCopy(g->renderer, ((Resource *)GetNthElement(lm->chapterOneTextures, SHIELD_RES)->data)->texture, NULL, &shieldplace);
    }
  }
}

//==============================================================================

static void DrawBackground(SDLManager *g, ListManager *lm, int elemInList)
{
  SDL_Rect visibleBackground = { 0 - g->camera.x, 0, 2*g->windowSize.w, g->windowSize.h };
  SDL_Rect visibleBackgroundNextTile = { 2*g->windowSize.w - g->camera.x, 0, 2*g->windowSize.w, g->windowSize.h };
  if(visibleBackground.x < -2*g->windowSize.w) {
    printf("flip!\n");
    g->camera.x = 0;
  }
  SDL_RenderCopy(g->renderer, ((Resource *)GetNthElement(lm->chapterOneTextures, elemInList)->data)->texture, NULL, &visibleBackground);
  SDL_RenderCopy(g->renderer, ((Resource *)GetNthElement(lm->chapterOneTextures, elemInList)->data)->texture, NULL, &visibleBackgroundNextTile);
}

//==============================================================================

static void DrawTopBar(SDLManager *g, ListManager *lm)
{
  SDL_Rect blackTopBar = { 0, 0, g->windowSize.w, TOP_OFFSET };
  SDL_RenderCopy(g->renderer, ((Resource *)GetNthElement(lm->chapterOneTextures, TOPBAR_RES)->data)->texture, NULL, &blackTopBar);
}

//==============================================================================

static void DrawIntroObjectivesOverlay(SDLManager * g, ControlManager * cm, ListManager * lm, ChapterOne *t)
{
  int w,h;
  SLList * iterator; int i;
  SDL_Rect textplace;
  if(SDL_PollEvent(&g->event)) {
    if(SDL_KEYDOWN == g->event.type) {
      if(g->event.key.keysym.sym == cm->Controls[CTRL_INTERACT].keyTrigger) t->onIntroScreen = 0;
    }
  }
  textplace.y = g->windowSize.h/4; // we set x, width and height later when we have texture ready
  for(iterator = lm->chapterOneObjectives, i = 0; iterator; iterator = iterator->next, i++) {
    SDL_QueryTexture(((Resource *)GetNthElement(lm->chapterOneObjectives, i)->data)->texture, NULL, NULL, &w, &h);
    textplace.x = g->windowSize.w/2 - w/2; textplace.y += (h+5); textplace.w = w; textplace.h = h;
    SDL_RenderCopy(g->renderer, ((Resource *)GetNthElement(lm->chapterOneObjectives, i)->data)->texture, NULL, &textplace);
  }
  SDL_SetRenderDrawColor(g->renderer, 0, 0, 0, 100);
  SDL_SetRenderDrawBlendMode(g->renderer, SDL_BLENDMODE_BLEND);
  SDL_RenderFillRect(g->renderer, NULL);
}

//==============================================================================

static void DrawPauseOverlay(SDLManager *g)
{
  SDL_Texture * helper;
  int w, h;
  helper = CreateTextTexture(g, "<span font='24' foreground='white'><b>PAUSE</b></span>", g->windowSize.w/2, 60);
  SDL_QueryTexture(helper, NULL, NULL, &w, &h);
  SDL_Rect helperrect = { g->windowSize.w/2 - w/2, g->windowSize.h/2 - h/2, w, h };
  SDL_RenderCopy(g->renderer, helper, NULL, &helperrect);
  SDL_SetRenderDrawColor(g->renderer, 0, 0, 0, 100);
  SDL_SetRenderDrawBlendMode(g->renderer, SDL_BLENDMODE_BLEND);
  SDL_RenderFillRect(g->renderer, NULL);
}

//==============================================================================
static void DrawChapterOneFrame(SDLManager * g, ControlManager * cm, ListManager * lm, ChapterOne *t)
{
  SLList * iterator;
  
  DrawBackground(g, lm, SPACE_RES);
  for(iterator = lm->chapterOneHeroes; iterator; iterator = iterator->next) ((Flying*)iterator->data)->fm->Draw(g, iterator);
  for(iterator = lm->chapterOneEnemies; iterator; iterator = iterator->next) ((Flying*)iterator->data)->fm->Draw(g, iterator);
  for(iterator = lm->chapterOneBosses; iterator; iterator = iterator->next) ((Flying*)iterator->data)->fm->Draw(g, iterator);
  for(iterator = lm->chapterOneExplosions; iterator; iterator = iterator->next) ((Explosion*)iterator->data)->em->Draw(g, iterator);
  for(iterator = lm->chapterOnePickables; iterator; iterator = iterator->next) ((Pickable*)iterator->data)->pm->Draw(g, iterator);
  for(iterator = lm->chapterOneMissiles; iterator; iterator = iterator->next) ((Missile*)iterator->data)->mm->Draw(g, iterator);
  for(iterator = lm->chapterOneEnemyMissiles; iterator; iterator = iterator->next) ((Missile*)iterator->data)->mm->Draw(g, iterator);
  
  DrawTopBar(g, lm);
  DrawHealth(g, lm);

  if(t->onIntroScreen)
    DrawIntroObjectivesOverlay(g, cm, lm, t);  
  if(t->Paused)
    DrawPauseOverlay(g);
  if(!t->Paused) // this kinda looks connectable (is that even a word?), doesn't it?
    for(iterator = lm->chapterOneExplosions; iterator; iterator = iterator->next) ((Explosion*)iterator->data)->em->AdvanceFrame(iterator);
    
  //ShowHitBoxesFlying(g, t, t->lm->chapterOneEnemies);
  //ShowHitBoxesFlying(g, t, t->lm->chapterOneBosses);
  //ShowHitBoxesFlying(g, t, t->lm->chapterOneHeroes);
}
//==============================================================================
//==============================================================================
//==============================================================================

static void ShipFly(SDLManager *g, SLList * who, enum Direction direction)
{
  int speed = SHIP_SPEED;
  Flying * hero;
  if(who) hero = ((Flying *) who->data);
  switch(direction) {
  case RIGHT:
    hero->o.position.x += speed;
    if(hero->o.position.x >= g->windowSize.w - hero->o.position.w) hero->o.position.x = g->windowSize.w - hero->o.position.w;
    // no camera move!
    break;
  case LEFT:
    hero->o.position.x -= speed;
    if(hero->o.position.x < 0) hero->o.position.x = 0;
    // no camera move!
    break;
  case DOWN:
    hero->o.position.y += speed;
    if(hero->o.position.y >= g->windowSize.h - hero->o.position.h) hero->o.position.y = g->windowSize.h - hero->o.position.h;
    // no camera move!
    break;
  case UP:
    hero->o.position.y -= speed;
    if(hero->o.position.y < TOP_OFFSET) hero->o.position.y = TOP_OFFSET;
    // no camera move!
    break;
  }
}

//==============================================================================
static void InitStuff(SDLManager *g, ChapterOne *t)
{
  t->Paused = 0;
  t->totalDistance = 0;

  // init the camera
  g->camera.x = 0; g->camera.y = 0; g->camera.h = g->windowSize.h; g->camera.w = g->windowSize.w;
}

//==============================================================================

static int PlayerKeyboardStateArrayInput(SDLManager *g, ControlManager *cm, ChapterOne *t)
{
  const Uint8 *keyboardStateArray = SDL_GetKeyboardState(NULL);
  SDL_PollEvent(&g->event);
  if(SDL_KEYDOWN == g->event.type || SDL_KEYUP == g->event.type || SDL_TEXTINPUT == g->event.type) {
    if(!t->Paused && !t->onIntroScreen && // some controls are disabled during pause and briefing
       t->p1 && ((Flying*)t->p1->data)->behavior == NO_BEHAVIOR) { // player is also not in control during respawn "animation"
      if(keyboardStateArray[cm->Controls[CTRL_GORIGHT].scanTrigger])     ShipFly(g, t->p1, RIGHT);
      else if(keyboardStateArray[cm->Controls[CTRL_GOLEFT].scanTrigger]) ShipFly(g, t->p1, LEFT);
      if(keyboardStateArray[cm->Controls[CTRL_GODOWN].scanTrigger])      ShipFly(g, t->p1, DOWN);
      else if(keyboardStateArray[cm->Controls[CTRL_GOUP].scanTrigger])   ShipFly(g, t->p1, UP);
      if(keyboardStateArray[cm->Controls[CTRL_SHOOT].scanTrigger]) ShootingScheme(t->p1);
      //if(keyboardStateArray[SDL_SCANCODE_H]) g->SetWindowSize(g, 1024, 800);
    }
    if(SDL_KEYDOWN == g->event.type && keyboardStateArray[cm->Controls[CTRL_PAUSE].scanTrigger]) ((t->Paused) ? (t->Paused = 0) : (t->Paused = 1)); // pause should not be treated as a continuous signal, ew restrict its power to KEYDOWN event which makes it more reliable
    if(keyboardStateArray[cm->Controls[CTRL_QUIT].scanTrigger]) return 0;
  }
  return 1;
}

//==============================================================================

static void LoadTextures(SDLManager *g, ResourceManager *rm, ListManager *lm, int levelNum)
{
  SDL_Texture * helper;
  helper = CreateCairoShipTexture(g); //rm->LoadTexture(g, "res/ship.png");
  rm->AddResourceToList(rm, &lm->chapterOneTextures, helper, 0);
  helper = CreateSpaceTexture(g); // rm->LoadTexture(g, "res/space.png"); // CreateSpaceTexture(bg);
  rm->AddResourceToList(rm, &lm->chapterOneTextures, helper, 0);
  helper = CreateMissileTexture(g); //rm->LoadTexture(g, "res/basicmissile.png");
  rm->AddResourceToList(rm, &lm->chapterOneTextures, helper, 0);
  helper = CreateShieldTexture(g); //rm->LoadTexture(g, "res/shield.png");
  rm->AddResourceToList(rm, &lm->chapterOneTextures, helper, 0);
  helper = CreateCairoEnemyTexture(g); //rm->LoadTexture(g, "res/basicenemy.png");
  rm->AddResourceToList(rm, &lm->chapterOneTextures, helper, 0);
  helper = CreateBoss1Texture(g); // rm->LoadTexture(g, "res/boss1.png");
  rm->AddResourceToList(rm, &lm->chapterOneTextures, helper, 0);
  helper = CreateCrateTexture(g); //rm->LoadTexture(g, "res/crate.png");
  rm->AddResourceToList(rm, &lm->chapterOneTextures, helper, 3, 
                       0,  0, 15, 15,
                       15, 0, 15, 15,
                       30, 0, 15, 15);
  helper = CreateLivesTexture(g); //rm->LoadTexture(g, "res/shipicon.png");
  rm->AddResourceToList(rm, &lm->chapterOneTextures, helper, 0);
  helper = CreateRocketTexture(g); //rm->LoadTexture(g, "res/rocket.png");
  rm->AddResourceToList(rm, &lm->chapterOneTextures, helper, 0);
  helper = CreateCannonTexture(g); // rm->LoadTexture(g, "res/cannon.png");
  rm->AddResourceToList(rm, &lm->chapterOneTextures, helper, 2, 
                       0,  0, 32, 32,
                       32, 0, 32, 32);
  helper = CreateTopBarTexture(g); // rm->LoadTexture(g, "res/topbar.png");
  rm->AddResourceToList(rm, &lm->chapterOneTextures, helper, 0);
  helper = CreateFiremineTexture(g); // rm->LoadTexture(g, "res/firemine.png");
  rm->AddResourceToList(rm, &lm->chapterOneTextures, helper, 2,
                       0,  0, 30, 30,
                       30, 0, 30, 30);
  helper = CreateFirewallTexture(g); // rm->LoadTexture(g, "res/firewall.png");
  rm->AddResourceToList(rm, &lm->chapterOneTextures, helper, 0);

  helper = CreateExplosionTexture(g); //rm->LoadTexture(g, "res/explosion.png");
  rm->AddResourceToList(rm, &lm->chapterOneTextures, helper, 23,
                       0,   0, 64, 64,    64,   0, 64, 64,    128,   0, 64, 64,    192,   0, 64, 64,    256,   0, 64, 64,
                       0,  64, 64, 64,    64,  64, 64, 64,    128,  64, 64, 64,    192,  64, 64, 64,    256,  64, 64, 64,
                       0, 128, 64, 64,    64, 128, 64, 64,    128, 128, 64, 64,    192, 128, 64, 64,    256, 128, 64, 64,
                       0, 192, 64, 64,    64, 192, 64, 64,    128, 192, 64, 64,    192, 192, 64, 64,    256, 192, 64, 64,
                       0, 256, 64, 64,    64, 256, 64, 64,    128, 256, 64, 64);

  switch(levelNum) {
  case 1:
    rm->AddTextsToList(rm, g, &lm->chapterOneObjectives, GetText(MISSION1_BRIEFING));
    break;
  case 2:
    rm->AddTextsToList(rm, g, &lm->chapterOneObjectives, GetText(MISSION2_BRIEFING));
    break;
  default:
    return;
  }  
}

//==============================================================================

static void CleanMarked(SLList ** list)
{
  SLList * iterator = *list;
  SLList * tmp;
  while(iterator) {
    if(iterator->markForDeletion) {
      tmp = iterator->next;
      DeleteFromList(list, iterator);
      iterator = tmp;
    } else {
      iterator = iterator->next;
    }
  }
}

//==============================================================================

static void CleanUpChapterOne(ListManager * lm)
{
  CleanMarked(&lm->chapterOneTextures);
  CleanMarked(&lm->chapterOneEnemies);
  CleanMarked(&lm->chapterOneMissiles);
  CleanMarked(&lm->chapterOneEnemyMissiles);
  CleanMarked(&lm->chapterOneHeroes);
  CleanMarked(&lm->chapterOnePickables);
  CleanMarked(&lm->chapterOneBosses);
}

//==============================================================================


static int ExecuteMainLoop(SDLManager *g, ControlManager *cm, MapsManager * mam, ExplosionManager * em, ListManager * lm, ChapterOne *t, int * returnCode, unsigned int * timer)
{
  int chapterOneLoop = 1;
  while(chapterOneLoop) {
    if(SDL_GetTicks() - *timer > 60) {
      *timer = SDL_GetTicks();
      HandleChapterOneFrame(g, t, mam, lm); // perform all sorts of updates to the scene
      CleanUpChapterOne(lm); // cleanup elements marked for deletion
      DrawChapterOneFrame(g, cm, lm, t); // draw the scene
      SDL_RenderPresent(g->renderer);
      if((*returnCode = SimplifiedExplosionManager(g, em, lm))) chapterOneLoop = 0; // break out of the loop and enter finishing screen
      
      if(!PlayerKeyboardStateArrayInput(g, cm, t)) return 0; //get keyboardStateArrayStateArray input, if quit key is pressed, function will return 0
                                               // if that happens this function must also quit on the spot
    }
  } // chapterOneLoop
  return 1;
}

//==============================================================================

static void VictoryAnimation(SDLManager *g, ControlManager *cm, MapsManager * mam, ListManager * lm, ChapterOne *t, unsigned int *timer)
{
  int flyForABit = 20;
  while(((Flying *)t->p1->data)->o.position.x < g->windowSize.w) { // now control darkness level with chapterOneLoop
    if(SDL_GetTicks() - *timer > 60) {
      *timer = SDL_GetTicks();
      CleanUpChapterOne(lm); // cleanup elements marked for deletion
      HandleChapterOneFrame(g, t, mam, lm); // perform all sorts of updates to the scene
      DrawChapterOneFrame(g, cm, lm, t);
      SDL_RenderPresent(g->renderer);
      if(flyForABit-- < 0) ((Flying *)t->p1->data)->o.position.x += 20;
    }
  }
}

//==============================================================================

static int FadeToBlackAnim(SDLManager *g, ControlManager *cm, ListManager * lm, ChapterOne *t, unsigned int *timer)
{
  int fadeToBlack = 0;
  while(fadeToBlack < 255) {
    if(SDL_GetTicks() - *timer > 60) {
      *timer = SDL_GetTicks();
      CleanUpChapterOne(lm); // cleanup elements marked for deletion
      DrawChapterOneFrame(g, cm, lm, t);
      SDL_SetRenderDrawColor(g->renderer, 0, 0, 0, fadeToBlack);
      fadeToBlack += 3;
      SDL_SetRenderDrawBlendMode(g->renderer, SDL_BLENDMODE_BLEND);
      SDL_RenderFillRect(g->renderer, NULL);
      SDL_RenderPresent(g->renderer);
      const Uint8 *keyboardStateArray = SDL_GetKeyboardState(NULL);
      SDL_PollEvent(&g->event);
      if(SDL_KEYDOWN == g->event.type || SDL_KEYUP == g->event.type) {
        if(keyboardStateArray[cm->Controls[CTRL_QUIT].scanTrigger]) return 0;
      }
    }
  }
  return 1;
}

//==============================================================================

static int BitOfBlack(SDLManager *g, ControlManager *cm, unsigned int *timer)
{
  // a bit of black screen
  int darknessTime = 30;
  while(darknessTime) {
    if(SDL_GetTicks() - *timer > 60) {
      *timer = SDL_GetTicks();
      darknessTime--;
      SDL_SetRenderDrawColor(g->renderer, 0, 0, 0, 255);
      SDL_RenderFillRect(g->renderer, NULL);
      SDL_RenderPresent(g->renderer);
      const Uint8 *keyboardStateArray = SDL_GetKeyboardState(NULL);
      SDL_PollEvent(&g->event);
      if(SDL_KEYDOWN == g->event.type || SDL_KEYUP == g->event.type) {
        if(keyboardStateArray[cm->Controls[CTRL_QUIT].scanTrigger]) return 0;
      }
    }          
  }
  return 1;
}

//==============================================================================

static int ExecuteEndingAnimations(SDLManager *g, ControlManager *cm, MapsManager *mam, ListManager *lm, ChapterOne *t, const int returnCode, unsigned int timer)
{
  if(returnCode == 2)
    VictoryAnimation(g, cm, mam, lm, t, &timer);    // quick victory animation
  if(returnCode != 2)
    if(!FadeToBlackAnim(g, cm, lm, t, &timer)) return 0;  // fade to black if dead
  
  if(!BitOfBlack(g, cm, &timer)) return 0; // show black screen for a bit
  return 1;
}

//==============================================================================

static int ChapterOneExecute(SDLManager *g, ControlManager *cm, ResourceManager *rm, MapsManager *mam, ListManager *lm, MissileManager *mm, FlyingManager *fm, ExplosionManager *em, PickableManager *pm, ChapterOne *t, int levelNum)
{
  // fill with zeros or init stuff
  InitStuff(g, t);

  // load sounds - soundmanager?
  // sounds withdrawn for now
  
  // load all textures
  LoadTextures(g, rm, lm, levelNum);

  // setup our hero ladies and gentlemen
  fm->AddPreset(fm, g, lm->chapterOneTextures, &lm->chapterOneHeroes, &lm->chapterOneMissiles, PLAYER1);
  if(lm->chapterOneHeroes) t->p1 = lm->chapterOneHeroes;  // and point to him
  //if(t->lm->chapterOneHeroes->next) t->p2 = t->lm->chapterOneHeroes->next; 

  // Draw Intro Screen
  t->onIntroScreen = 1;
  //DrawIntroObjectivesOverlay(g, t);

  // couple of passed variables
  unsigned int timer = 0;
  int returnCode = 0;

  // main loop
  if(!ExecuteMainLoop(g, cm, mam, em, lm, t, &returnCode, &timer)) return 0; // see PlayerKeyboardStateArrayInput (in ExecuteMainLoop) comment
  
  // main loop ended (i.e. player died/won/quit), go for ending
  if(!ExecuteEndingAnimations(g, cm, mam, lm, t, returnCode, timer)) return 0; // see PlayerKeyboardStateArrayInput (in ExecuteMainLoop) comment

  // return 
  return returnCode; //success
}

//==============================================================================
static void ChapterOneExit(ListManager * lm)
{
  //Mix_FreeChunk( t->chapteronesounds[0] );
  //Mix_FreeChunk( t->chapteronesounds[1] );
  SLList * iterator;
  
  while((iterator = lm->chapterOneTextures) != NULL) DeleteFromList(&lm->chapterOneTextures, iterator);
  while((iterator = lm->chapterOneEnemies) != NULL) DeleteFromList(&lm->chapterOneEnemies, iterator);
  while((iterator = lm->chapterOneMissiles) != NULL) DeleteFromList(&lm->chapterOneMissiles, iterator);
  while((iterator = lm->chapterOneEnemyMissiles) != NULL) DeleteFromList(&lm->chapterOneEnemyMissiles, iterator);
  while((iterator = lm->chapterOneHeroes) != NULL) DeleteFromList(&lm->chapterOneHeroes, iterator);
  while((iterator = lm->chapterOnePickables) != NULL) DeleteFromList(&lm->chapterOnePickables, iterator);
  while((iterator = lm->chapterOneObjectives) != NULL) DeleteFromList(&lm->chapterOneObjectives, iterator);
  while((iterator = lm->chapterOneExplosions) != NULL) DeleteFromList(&lm->chapterOneExplosions, iterator);
  while((iterator = lm->chapterOneBosses) != NULL) DeleteFromList(&lm->chapterOneBosses, iterator);
}

//==============================================================================
void ChapterOneInit(ChapterOne * t)
{
  t->OnExecute = &ChapterOneExecute;
  t->OnExit = &ChapterOneExit;
}

