/*
    This file is a part of cargo - a space shooter game
    Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _CHAPTERONE_H_
#define _CHAPTERONE_H_

#include "MapsManager.h"
#include "MissileManager.h"
#include "FlyingManager.h"
#include "ExplosionManager.h"
#include "PickableManager.h"
#include "ControlManager.h"

//==============================================================================

enum Direction {
  LEFT,
  RIGHT,
  UP,
  DOWN,
};

typedef struct _ChapterOne {  
  SLList * p1; // no list! pure pointer
  char Paused;
  char onIntroScreen;
  int totalDistance;

  int (*OnExecute)(SDLManager *g, ControlManager *cm, ResourceManager *rm, MapsManager *mam, ListManager * lm, MissileManager *mm, FlyingManager *fm, ExplosionManager *em, PickableManager *pm, struct _ChapterOne *t, int levelNum);
  void (*OnExit)(ListManager *);
} ChapterOne;

//==============================================================================
void ChapterOneInit(ChapterOne *t);

// !!!!!

#endif
