/*
  This file is a part of cargo - a space shooter game
  Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Collisions.h"
#include "Behaviors.h"

static SDL_Rect RelocateHitBox(SDL_Rect * hitbox, SDL_Rect * pos);
static void ManDown(ExplosionManager *em, ListManager * lm, SLList * eiterator);
static void ReincarnatePlayer(SDLManager *g, Flying *player);

static SDL_Rect RelocateHitBox(SDL_Rect * hitbox, SDL_Rect * pos)
{
  SDL_Rect rb = *hitbox;
  rb.x += pos->x;
  rb.y += pos->y;
  return rb;
}



static void ManDown(ExplosionManager *em, ListManager * lm, SLList * eiterator)
{
  em->Add(em, &lm->chapterOneExplosions, ((Resource *)GetNthElement(lm->chapterOneTextures, EXPLOSION_RES)->data), ((Flying *)eiterator->data)->o.position);
  //Mix_PlayChannel(-1, t->explosionsound, 0);
}

static void ReincarnatePlayer(SDLManager *g, Flying *player)
{
  player->attackType = ATTACK_NORMAL;
  player->health = 3;
  player->immunity = 40;
  player->o.position.y = g->windowSize.h/2; 
  player->o.position.x = - player->o.position.w; // out of the screen
  player->behavior = BEHAVIOR_HERO;
  player->weaponReloadTime = 3;
}

// *********************
void ShowHitBoxesFlying(SDLManager *g, SLList *l)
{
  SLList * eiterator, * hiterator;
  Flying * currentEnemy;
  Hitbox * currentHitbox;
  SDL_Rect relochbox;
  for(eiterator = l; eiterator; eiterator = eiterator->next) {
    currentEnemy = ((Flying *)eiterator->data);
    for(hiterator = currentEnemy->o.hitbox; hiterator; hiterator = hiterator->next) {
      currentHitbox = ((Hitbox *)hiterator->data);
      // relocate hitboxes
      relochbox = RelocateHitBox(&currentHitbox->hb, &currentEnemy->o.position);
      if(currentHitbox->damagable)
        SDL_SetRenderDrawColor(g->renderer, 255, 0, 0, 255);
      else
        SDL_SetRenderDrawColor(g->renderer, 0, 0, 255, 255);
      SDL_RenderDrawRect(g->renderer, &relochbox);
    }
  }
}

static int CheckLives(SDLManager * g, SLList * eiterator)
{
  Flying * current = ((Flying *)eiterator->data);
  if(current->lives <= 0) {
    if(current->behaviorAfterDeath != NO_BEHAVIOR) {
        BehaviorSelect(current->behaviorAfterDeath, g, eiterator);
    }
    return 1;
  }
  return 0;
}

 static int CheckHealthDecLife(SDLManager * g, ExplosionManager * em, ListManager * lm, SLList * eiterator)
{
  Flying * current = ((Flying *)eiterator->data);
  if(current->health <= 0) {
    ManDown(em, lm, eiterator);
    current->lives--;
    if(CheckLives(g, eiterator)) return 1; // if someone died, return from here now
    if(current->listhead == &lm->chapterOneHeroes) ReincarnatePlayer(g, current);
  }
  return 0;
}

static int HandleCollision(SDLManager *g, ExplosionManager * em, ListManager * lm, 
                           SLList * miterator, SLList * eiterator, SLList * hiterator)
{
  Flying * currentEnemy = ((Flying *)eiterator->data);
  Hitbox * currentHitbox = ((Hitbox *)hiterator->data);

  miterator->markForDeletion = 1;
  if(currentHitbox->damagable) {  // do we actually have a damaging hit?
    currentEnemy->ishit = 1;
    currentEnemy->health--;
    if(CheckHealthDecLife(g, em, lm, eiterator)) return 1;
  }
  return 0;
}

static int CheckCollision(SDLManager * g, ExplosionManager * em, ListManager * lm, 
                          SLList *eiterator, SLList *miterator)
{
  SLList * hiterator;
  Hitbox * currentHitbox;
  SDL_Rect relocatedHitbox;
  Missile * currentMissile = ((Missile *)miterator->data);
  Flying * currentEnemy = ((Flying *)eiterator->data);
  // hitbox is stored positioned at 0,0. We need to relocate it to the current x,y position
  SDL_Rect result; //we can't pass null as third SDL_InersectRect param, because
                   //it will always return FALSE. Create this rect to pass it.
                   //we completely ignore it btw.
  for(hiterator = currentEnemy->o.hitbox; hiterator; hiterator = hiterator->next) {
    currentHitbox = ((Hitbox *)hiterator->data);
    relocatedHitbox = RelocateHitBox(&currentHitbox->hb, &currentEnemy->o.position);
    if(!currentEnemy->immunity &&
       SDL_IntersectRect(&currentMissile->o.position, &relocatedHitbox, &result))
      if(HandleCollision(g, em, lm, miterator, eiterator, hiterator)) return 1;
  }
  return 0;
}

static int HandleCrash(SDLManager *g, ExplosionManager * em, ListManager * lm, 
                       SLList * piterator, SLList * eiterator, SLList * enemyHiterator, SLList * playerHiterator)
{
  Flying * currentEnemy = ((Flying *)eiterator->data);
  Flying * currentPlayer = ((Flying *)piterator->data);
  Hitbox * enemyHitbox = ((Hitbox *)enemyHiterator->data);
  Hitbox * playerHitbox = ((Hitbox *)playerHiterator->data);

  if(enemyHitbox->damagable) {  // do we actually have a damaging hit?
    if(currentEnemy->listhead == &lm->chapterOneBosses) currentEnemy->health -= currentPlayer->health;
    else currentEnemy->health = 0; // only for "casual" enemies
  }
  if(playerHitbox->damagable) {  // do we actually have a damaging hit?
    currentPlayer->health = 0;
  }

  if(CheckHealthDecLife(g, em, lm, piterator)) return 1;
  if(CheckHealthDecLife(g, em, lm, eiterator)) {
    if(currentEnemy->listhead == &lm->chapterOneBosses)
      return 2;
    else
      return 0;
  }
  return 0;
}

static int CheckCrash(SDLManager * g, ExplosionManager * em, ListManager * lm,
                      SLList *eiterator, SLList *piterator)
{
  SLList * enemyHiterator;
  SLList * playerHiterator;
  Hitbox * currentEnemyHitbox;
  Hitbox * currentPlayerHitbox;
  SDL_Rect relocatedEnemyHitbox;
  SDL_Rect relocatedPlayerHitbox;
  Flying * currentPlayer = ((Flying *)piterator->data);
  Flying * currentEnemy = ((Flying *)eiterator->data);
  int retval;
  // hitbox is stored positioned at 0,0. We need to relocate it to the current x,y position
  SDL_Rect result; //we can't pass null as third SDL_InersectRect param, because
                   //it will always return FALSE. Create this rect to pass it.
                   //we completely ignore it btw.
  for(enemyHiterator = currentEnemy->o.hitbox; enemyHiterator; enemyHiterator = enemyHiterator->next) {
    currentEnemyHitbox = ((Hitbox *)enemyHiterator->data);
    relocatedEnemyHitbox = RelocateHitBox(&currentEnemyHitbox->hb, &currentEnemy->o.position);
    for(playerHiterator = currentPlayer->o.hitbox; playerHiterator; playerHiterator = playerHiterator->next) {
      currentPlayerHitbox = ((Hitbox *)playerHiterator->data);
      relocatedPlayerHitbox = RelocateHitBox(&currentPlayerHitbox->hb, &currentPlayer->o.position);
      if(!currentPlayer->immunity &&
         SDL_IntersectRect(&relocatedEnemyHitbox, &relocatedPlayerHitbox, &result))
        if((retval = HandleCrash(g, em, lm, piterator, eiterator, enemyHiterator, playerHiterator))) return retval;
    }
  }
  return 0;
}


static int HandlePickup(SLList * pickupIterator, SLList * playerIterator)
{
  Pickable * currentPickable = ((Pickable *)pickupIterator->data);
  Flying * currentPlayer = ((Flying *)playerIterator->data);
  pickupIterator->markForDeletion = 1;
  if(currentPickable->what == HEALTH) currentPlayer->health++;
  if(currentPickable->what == WEAPON_DOUBLE) { currentPlayer->attackType = ATTACK_DOUBLE; currentPlayer->weaponReloadTime = 3; }
  if(currentPickable->what == WEAPON_TRIPLE) { currentPlayer->attackType = ATTACK_TRIPLE; currentPlayer->weaponReloadTime = 3; }
  if(currentPickable->what == WEAPON_TURBO) { currentPlayer->attackType = ATTACK_ROCKET; currentPlayer->weaponReloadTime = 3; }
  if(currentPickable->what == LIFE) currentPlayer->lives++;
  return 0;
}

static int CheckPickup(SDLManager * g,
                       SLList *pickupIterator, SLList *playerIterator)
{
  SLList * hiterator;
  Hitbox * currentHitbox;
  SDL_Rect relocatedHitbox;
  Pickable * currentPickable = ((Pickable *)pickupIterator->data);
  Flying * currentPlayer = ((Flying *)playerIterator->data);
  // hitbox is stored positioned at 0,0. We need to relocate it to the current x,y position
  SDL_Rect result; //we can't pass null as third SDL_InersectRect param, because
                   //it will always return FALSE. Create this rect to pass it.
                   //we completely ignore it btw.
  for(hiterator = currentPlayer->o.hitbox; hiterator; hiterator = hiterator->next) {
    currentHitbox = ((Hitbox *)hiterator->data);
    relocatedHitbox = RelocateHitBox(&currentHitbox->hb, &currentPlayer->o.position);
    if(SDL_IntersectRect(&currentPickable->o.position, &relocatedHitbox, &result))
      HandlePickup(pickupIterator, playerIterator);
  }
  return 0;
}

int SimplifiedExplosionManager(SDLManager *g, ExplosionManager *em, ListManager *lm)
{
  int retval;
  SLList * miterator, * eiterator;

  // missiles shot by players
  for(miterator = lm->chapterOneMissiles; miterator; miterator = miterator->next) {
    // player shoots the enemy
    for(eiterator = lm->chapterOneEnemies; eiterator; eiterator = eiterator->next) {
      CheckCollision(g, em, lm, eiterator, miterator);
    }
    // player shoots the boss
    for(eiterator = lm->chapterOneBosses; eiterator; eiterator = eiterator->next) {
      if(CheckCollision(g, em, lm, eiterator, miterator)) return 2;
    }
  }

  // a lot of stuff may happen to our heroes
  for(miterator = lm->chapterOneHeroes; miterator; miterator = miterator->next) {
    //getting shot by enemies
    for(eiterator = lm->chapterOneEnemyMissiles; eiterator; eiterator = eiterator->next) {
      if(CheckCollision(g, em, lm, miterator, eiterator)) return 3;
    }

    // crashes with "normal" enemies
    for(eiterator = lm->chapterOneEnemies; eiterator; eiterator = eiterator->next) {
      if(CheckCrash(g, em, lm, eiterator, miterator)) return 3;
    }

    // crashes with bosses - they cannot die unless they have little health
    // for the player it is always a suicide
    for(eiterator = lm->chapterOneBosses; eiterator; eiterator = eiterator->next) {
      if((retval = CheckCrash(g, em, lm, eiterator, miterator))) return retval;
    }
  
    // collecting pickups
    for(eiterator = lm->chapterOnePickables; eiterator; eiterator = eiterator->next) {
      CheckPickup(g, eiterator, miterator);
    }
  }
  return 0;
}

//==============================================================================
