/*
    This file is a part of cargo - a space shooter game
    Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//==============================================================================
#include "CommonManager.h"
#include "MissileManager.h"
#include "PickableManager.h"
#include "ExplosionManager.h"
#include "FlyingManager.h"

void AddHitBox(SLList ** list, int x, int y, int w, int h, char damagable)
{
  Hitbox hb;
  SDL_Rect r = { x, y, w, h };
  hb.hb = r;
  hb.damagable = damagable;
  AddToBackList(list, &hb, sizeof(Hitbox), HITBOX_T, NULL);
}

void AddHitBoxR(SLList ** list, SDL_Rect * rect, char damagable)
{
  AddHitBox(list, rect->x, rect->y, rect->w, rect->h, damagable);
}

void DeleteInternalCore(SLList * element)
{
  SLList * iterator;
  void * data = element->data;
  switch(element->dataType) {
  case PICKABLE_T:
    while((iterator = ((Pickable *)data)->o.hitbox) != NULL) DeleteFromList(&((Pickable*)data)->o.hitbox, iterator);
    break;
  case EXPLOSION_T:
    while((iterator = ((Explosion *)data)->o.hitbox) != NULL) DeleteFromList(&((Explosion*)data)->o.hitbox, iterator);
    break;
  case FLYING_T:
    while((iterator = ((Flying *)data)->o.hitbox) != NULL) DeleteFromList(&((Flying*)data)->o.hitbox, iterator);
    break;
  case MISSILE_T:
    while((iterator = ((Missile *)data)->o.hitbox) != NULL) DeleteFromList(&((Missile*)data)->o.hitbox, iterator);
    break;
  case HITBOX_T:
    // Hitbox type has no internal alloc
    break;
  default:
    printf("UNKNOWN TYPE!!!!!!!!!!!!!!!\n");
    break;
  }
}

