/*
    This file is a part of cargo - a space shooter game
    Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _COMMONMANAGER_H_
#define _COMMONMANAGER_H_

#include "SLList.h"

#include "ResourceManager.h"

#include <SDL2/SDL.h>

//==============================================================================
typedef enum _Type {
  RESOURCE_T,
  SDL_RECT_T,
  CORE_T,
  EXPLOSION_T,
  FLYING_T,
  MISSILE_T,
  PICKABLE_T,
  HITBOX_T,
} Type;

typedef struct _Core {
  Resource * resource; // texture and clip
  SDL_Rect position; // position and size
  SLList * currentFrame; // current frame if texture animated
  SLList * hitbox; // Hitbox type - Where the object may be hit (and if it takes damage)
} Core;  

typedef struct _Hitbox {
  SDL_Rect hb;
  char damagable;
} Hitbox;

void AddHitBox(SLList ** list, int x, int y, int w, int h, char damagable);
void AddHitBoxR(SLList ** list, SDL_Rect * rect, char damagable);
void DeleteInternalCore(SLList * element);

#endif //_COMMONMANAGER_H_
