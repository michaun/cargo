/*
    This file is a part of cargo - a space shooter game
    Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//==============================================================================
#include "ControlManager.h"

static void InitControls(ControlManager * g)
{
  g->Controls[CTRL_GOUP].scanTrigger = SDL_SCANCODE_UP;         g->Controls[CTRL_GOUP].keyTrigger = SDLK_UP;
  g->Controls[CTRL_GODOWN].scanTrigger = SDL_SCANCODE_DOWN;     g->Controls[CTRL_GODOWN].keyTrigger = SDLK_DOWN;
  g->Controls[CTRL_GOLEFT].scanTrigger = SDL_SCANCODE_LEFT;     g->Controls[CTRL_GOLEFT].keyTrigger = SDLK_LEFT;
  g->Controls[CTRL_GORIGHT].scanTrigger = SDL_SCANCODE_RIGHT;   g->Controls[CTRL_GORIGHT].keyTrigger = SDLK_RIGHT;
  g->Controls[CTRL_SHOOT].scanTrigger = SDL_SCANCODE_RCTRL;     g->Controls[CTRL_SHOOT].keyTrigger = SDLK_RCTRL;
  g->Controls[CTRL_PAUSE].scanTrigger = SDL_SCANCODE_P;         g->Controls[CTRL_PAUSE].keyTrigger = SDLK_p;
  g->Controls[CTRL_INTERACT].scanTrigger = SDL_SCANCODE_RETURN; g->Controls[CTRL_INTERACT].keyTrigger = SDLK_RETURN;
  g->Controls[CTRL_QUIT].scanTrigger = SDL_SCANCODE_Q;          g->Controls[CTRL_QUIT].keyTrigger = SDLK_q;
}

void ChangeControl(Control * c, SDL_Keycode keycode, SDL_Scancode scancode)
{
  c->keyTrigger = keycode;
  c->scanTrigger = scancode;
}

//==============================================================================
static int ControlManagerExecute(ControlManager * g)
{
  InitControls(g);
  return 0; //success
}

//==============================================================================
static void ControlManagerExit(ControlManager * g)
{
  // nothing to do
}
//==============================================================================
void ControlManagerInit(ControlManager * g)
{
  g->OnExecute = &ControlManagerExecute;
  g->OnExit = &ControlManagerExit;
  g->ChangeControl = &ChangeControl;
}
