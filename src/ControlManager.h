/*
    This file is a part of cargo - a space shooter game
    Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _CONTROLMANAGER_H_
    #define _CONTROLMANAGER_H_

#include <SDL2/SDL.h>

//==============================================================================

typedef enum _Ctrl {
  CTRL_GOUP,
  CTRL_GODOWN,
  CTRL_GOLEFT,
  CTRL_GORIGHT,
  CTRL_SHOOT,
  CTRL_PAUSE,
  CTRL_INTERACT,
  CTRL_QUIT,
  TOTAL_CONTROLS,
} Ctrl;

typedef struct _Control {
  //void (*command)(void);
  SDL_Scancode scanTrigger;
  SDL_Keycode keyTrigger;
} Control;

typedef struct _ControlManager {;
  Control Controls[TOTAL_CONTROLS];
  
  int (*OnExecute)(struct _ControlManager *);
  void (*OnExit)(struct _ControlManager *);
  void (*ChangeControl)(Control * c, SDL_Keycode, SDL_Scancode);
} ControlManager;

//==============================================================================
void ControlManagerInit(ControlManager *);

//==============================================================================

#endif
