/*
    This file is a part of cargo - a space shooter game
    Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//==============================================================================
#include "ExplosionManager.h"

static void AddExplosion(ExplosionManager * em, SLList ** list, Resource * explosionRes, SDL_Rect startingPos)
{
  printf("Add Explosion!\n");
  // new element things
  Explosion * new = malloc(sizeof(Explosion));
  new->o.resource = explosionRes;
  new->o.position = *((SDL_Rect *)explosionRes->clip->data);
  new->o.position.x = startingPos.x; //update x and y 
  new->o.position.y = startingPos.y;
  new->o.currentFrame = explosionRes->clip;
  new->listhead = list;
  new->em = em;
  new->o.hitbox = NULL;
  AddHitBoxR(&new->o.hitbox, &new->o.position, 1);
  AddToList(list, (void*)new, sizeof(Explosion), EXPLOSION_T, DeleteInternalCore);
  free(new); //add to list makes a copy of the element
}

static void HandleExplosions(SDLManager *g, SLList * iterator)
{
  Explosion * current = ((Explosion *)iterator->data);
  if(!current->o.currentFrame) iterator->markForDeletion = 1;
}

static void DrawExplosion(SDLManager *g, SLList * iterator)
{
  Explosion * current = ((Explosion *)iterator->data);
  if(current && current->o.currentFrame) {
    SDL_RenderCopy(g->renderer, current->o.resource->texture, ((SDL_Rect *)current->o.currentFrame->data), &current->o.position);
  }
}

static void AdvanceFrame(SLList * iterator)
{
  Explosion * current = ((Explosion *)iterator->data);
  if(current && current->o.currentFrame) {
    current->o.currentFrame = current->o.currentFrame->next;
  }
}

void InitExplosionManager(ExplosionManager * em)
{
  em->Add = &AddExplosion;
  em->Draw = &DrawExplosion;
  em->Handle = &HandleExplosions;
  em->AdvanceFrame = &AdvanceFrame;
}
