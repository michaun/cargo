/*
    This file is a part of cargo - a space shooter game
    Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//==============================================================================

#include "FlyingManager.h"
#include "MissileManager.h"
#include "Behaviors.h"

static void AddFlyingCommonCode(Flying ** new, FlyingManager * fm, int codename,
                                int startx, int starty, int desx, int desy, double angle, SLList ** targetlist, SLList ** associatedMissileList,
                                enum BehaviorScheme behavior, enum BehaviorScheme behaviorAfterDeath,                                char weaponReloadTime, enum AttackType attackType, char health,
                                char lives, char leaveWreckage)
{
  (*new)->id = 0;//(*debugId)++; // give unique id - more of a debugging thing
  (*new)->codename = codename;
  (*new)->weaponTimeout = 0;
  (*new)->immunity = 0;
  (*new)->o.position.y = starty;
  (*new)->o.position.x = startx;
  (*new)->behavior = behavior;
  (*new)->behaviorAfterDeath = behaviorAfterDeath;
  (*new)->weaponReloadTime = weaponReloadTime;
  (*new)->attackType = attackType;
  (*new)->health = health;
  (*new)->lives = lives;
  (*new)->leaveWreckage = leaveWreckage;
  (*new)->angle = angle;
 
  (*new)->listhead = NULL;
  (*new)->associatedMissileList = NULL;
  (*new)->ishit = 0;
  (desx == -1) ? ((*new)->desiredPosition.x = (*new)->o.position.x) : ((*new)->desiredPosition.x = desx);
  (desy == -1) ? ((*new)->desiredPosition.y = (*new)->o.position.y) : ((*new)->desiredPosition.y = desy);

  (*new)->o.hitbox = NULL;
  (*new)->listhead = targetlist;
  (*new)->associatedMissileList = associatedMissileList;
  (*new)->fm = fm;
}

static void AddFlyingHitbox(Flying **new, int hitboxnum, ...)
{
  va_list vl;
  SDL_Rect r;
  int damagable;
  va_start(vl, hitboxnum);
  for(int i = 0; i < hitboxnum; i++) {
    r.x = va_arg(vl, int);
    r.y = va_arg(vl, int);
    r.w = va_arg(vl, int);
    r.h = va_arg(vl, int);
    damagable = va_arg(vl, int);
    AddHitBox(&(*new)->o.hitbox, r.x, r.y, r.w, r.h, damagable);
  }
}

static void AddFlyingSpecific(Flying ** new, SDLManager *g, SLList * resourcelist, enum FlyingType ft)
{
  int resourceId;

  if(ENEMY == ft) resourceId = ENEMY_RES;
  else if(CANNON == ft) resourceId = CANNON_RES;
  else if(FIREMINE == ft) resourceId = FIREMINE_RES;
  else if(FIREMINEWALL == ft) resourceId = FIREWALL_RES;
  else if(BOSS == ft) resourceId = BOSS_RES;
  else if(HERO == ft) resourceId = SHIP_RES;
  
  (*new)->o.resource = ((Resource *)GetNthElement(resourcelist, resourceId)->data);
  (*new)->o.currentFrame = (*new)->o.resource->clip; // first frame should be here
  (*new)->o.position.w = ((SDL_Rect*)(*new)->o.currentFrame->data)->w;
  (*new)->o.position.h = ((SDL_Rect*)(*new)->o.currentFrame->data)->h;

  // only now, when we have full position rect, we can check for boundaries
  if((*new)->o.position.y + (*new)->o.position.h > g->windowSize.h) (*new)->o.position.y = g->windowSize.h - (*new)->o.position.h; 
  if((*new)->desiredPosition.y < TOP_OFFSET) (*new)->desiredPosition.y = TOP_OFFSET;
  
  switch(ft) {
  case ENEMY:
    AddFlyingHitbox(new, 1,
                    0, 0, (*new)->o.position.w, (*new)->o.position.h, 1);
    break;
  case CANNON:
    AddFlyingHitbox(new, 1,
                    0, 0, (*new)->o.position.w, (*new)->o.position.h, 1);
    break;
  case FIREMINE:
    AddFlyingHitbox(new, 1,
                    0, 0, (*new)->o.position.w, (*new)->o.position.h, 1);
    break;
  case FIREMINEWALL:
    AddFlyingHitbox(new, 1,
                    0, 0, (*new)->o.position.w, (*new)->o.position.h, 0);
    break;
  case BOSS:
    AddFlyingHitbox(new, 8,
                    60,  35, 135,  5, 1,
                    80,  30, 115,  5, 1,
                    100, 25,  95,  5, 1,
                    120, 20,  75,  5, 1,
                    140, 15,  55,  5, 1,
                    160, 10,  35,  5, 1,
                    180,  0,  15, 70, 1,
                    10,  40, 145, 70, 1);
    break;
  case HERO:
    AddFlyingHitbox(new, 1,
                    0, 0, (*new)->o.position.w, (*new)->o.position.h, 1);
    break;
  }
}

static void AddFlying(FlyingManager * fm, SDLManager * g, enum FlyingType ft,
                      int startx, int starty, int desx, int desy, double angle, SLList * resourcelist,
                      SLList ** targetlist, SLList ** associatedMissileList, enum BehaviorScheme behavior,
                      enum BehaviorScheme behaviorAfterDeath, int codename,
                      char weaponReloadTime, enum AttackType attackType,
                      char health, char lives, char leavewrekage)
{
  printf("Add Flying!\n");
  Flying * new = malloc(sizeof(Flying));

  AddFlyingCommonCode(&new, fm, codename, startx, starty, desx, desy, angle, targetlist, associatedMissileList, behavior, behaviorAfterDeath, weaponReloadTime, attackType, health, lives, leavewrekage); // fill in general fields
  AddFlyingSpecific(&new, g, resourcelist, ft); // fill in resource- and type-specific fields
  AddToList(new->listhead, (void*)new, sizeof(Flying), FLYING_T, DeleteInternalCore);
  free(new); //add to list makes a copy of the element
}

//================
static void EnemyRandUpDnPreset(FlyingManager * fm, SDLManager * g, SLList * resourcelist, SLList ** targetlist, SLList ** associatedMissileList)
{
  AddFlying(fm, g, ENEMY, g->windowSize.w, rand() % (g->windowSize.h), -1, -1, 180, resourcelist, targetlist, associatedMissileList, BEHAVIOR_ENEMY1, DESTROYED_OBJECT_BEHAVIOR, -1, 4, ATTACK_NORMAL, 3, 1, 0);
}

static void EnemyRandUpDnRocketPreset(FlyingManager * fm, SDLManager * g, SLList * resourcelist, SLList ** targetlist, SLList ** associatedMissileList)
{
  AddFlying(fm, g, ENEMY, g->windowSize.w, rand() % (g->windowSize.h), -1, -1, 180, resourcelist, targetlist, associatedMissileList, BEHAVIOR_ENEMY1, DESTROYED_OBJECT_BEHAVIOR, -1, 4, ATTACK_ROCKET, 3, 1, 0);
}

static void EnemyKamikazePreset(FlyingManager * fm, SDLManager * g, SLList * resourcelist, SLList ** targetlist, SLList ** associatedMissileList)
{
      AddFlying(fm, g, ENEMY, g->windowSize.w, rand() % (g->windowSize.h), -1, -1, 180, resourcelist, targetlist, associatedMissileList, BEHAVIOR_ENEMY2, DESTROYED_OBJECT_BEHAVIOR, -1, 4, ATTACK_NORMAL, 3, 1, 0);
}

static void EnemyDoubleKamikazePreset(FlyingManager * fm, SDLManager * g, SLList * resourcelist, SLList ** targetlist, SLList ** associatedMissileList)
{
      AddFlying(fm, g, ENEMY, g->windowSize.w, 1*(g->windowSize.h/4), -1, -1, 180, resourcelist, targetlist, associatedMissileList, BEHAVIOR_ENEMY2, DESTROYED_OBJECT_BEHAVIOR, -1, 4, ATTACK_NORMAL, 3, 1, 0);
      AddFlying(fm, g, ENEMY, g->windowSize.w, 3*(g->windowSize.h/4), -1, -1, 180, resourcelist, targetlist, associatedMissileList, BEHAVIOR_ENEMY2, DESTROYED_OBJECT_BEHAVIOR, -1, 4, ATTACK_NORMAL, 3, 1, 0);
}

static void EnemyRocketsWavePreset(FlyingManager * fm, SDLManager * g, SLList * resourcelist, SLList ** targetlist, SLList ** associatedMissileList)
{
      AddFlying(fm, g, ENEMY, g->windowSize.w, rand () % (g->windowSize.h), -1, -1, 180, resourcelist, targetlist, associatedMissileList, BEHAVIOR_ENEMY_WAVE, DESTROYED_OBJECT_BEHAVIOR, -1, 4, ATTACK_ROCKET, 3, 1, 0);
}

static void EnemyWavePreset(FlyingManager * fm, SDLManager * g, SLList * resourcelist, SLList ** targetlist, SLList ** associatedMissileList)
{
      AddFlying(fm, g, ENEMY, g->windowSize.w, rand () % (g->windowSize.h), -1, -1, 180, resourcelist, targetlist, associatedMissileList, BEHAVIOR_ENEMY_WAVE, DESTROYED_OBJECT_BEHAVIOR, -1, 4, ATTACK_NORMAL, 3, 1, 0);
}

static int EnemyTripleStaticCheckAvailability(FlyingManager * fm, SDLManager * g, SLList * resourcelist, SLList ** targetlist, SLList ** associatedMissileList)
{
  SLList * eiterator;
  Flying * current;
  for(eiterator = (*targetlist); eiterator; eiterator = eiterator->next) {
    current = ((Flying *)eiterator->data);
    if(current->behavior == BEHAVIOR_ENEMY3) return 0;
  }
  return 1;
}


static void EnemyTripleStaticPreset(FlyingManager * fm, SDLManager * g, SLList * resourcelist, SLList ** targetlist, SLList ** associatedMissileList)
{
      AddFlying(fm, g, ENEMY, g->windowSize.w, rand () % (g->windowSize.h), 3*(g->windowSize.w/4), 1*(g->windowSize.h/4), 180, resourcelist, targetlist, associatedMissileList, BEHAVIOR_ENEMY3, DESTROYED_OBJECT_BEHAVIOR, -1, 4, ATTACK_NORMAL, 3, 1, 0);
      AddFlying(fm, g, ENEMY, g->windowSize.w, rand () % (g->windowSize.h), 3*(g->windowSize.w/4), 2*(g->windowSize.h/4), 180, resourcelist, targetlist, associatedMissileList, BEHAVIOR_ENEMY3, DESTROYED_OBJECT_BEHAVIOR, -1, 4, ATTACK_NORMAL, 3, 1, 0);
      AddFlying(fm, g, ENEMY, g->windowSize.w, rand () % (g->windowSize.h), 3*(g->windowSize.w/4), 3*(g->windowSize.h/4), 180, resourcelist, targetlist, associatedMissileList, BEHAVIOR_ENEMY3, DESTROYED_OBJECT_BEHAVIOR, -1, 4, ATTACK_NORMAL, 3, 1, 0);
}

static void EnemyTripleStaticSwitchPreset(FlyingManager * fm, SDLManager * g, SLList * resourcelist, SLList ** targetlist, SLList ** associatedMissileList)
{
      AddFlying(fm, g, ENEMY, g->windowSize.w, rand () % (g->windowSize.h), 3*(g->windowSize.w/4), 1*(g->windowSize.h/4), 180, resourcelist, targetlist, associatedMissileList, BEHAVIOR_ENEMY3, AFTER_DEATH87, 87, 4, ATTACK_NORMAL, 3, 1, 0);
      AddFlying(fm, g, ENEMY, g->windowSize.w, rand () % (g->windowSize.h), 3*(g->windowSize.w/4), 2*(g->windowSize.h/4), 180, resourcelist, targetlist, associatedMissileList, BEHAVIOR_ENEMY3, DESTROYED_OBJECT_BEHAVIOR, 88, 4, ATTACK_NORMAL, 3, 1, 0);
      AddFlying(fm, g, ENEMY, g->windowSize.w, rand () % (g->windowSize.h), 3*(g->windowSize.w/4), 3*(g->windowSize.h/4), 180, resourcelist, targetlist, associatedMissileList, BEHAVIOR_ENEMY3, DESTROYED_OBJECT_BEHAVIOR, 89, 4, ATTACK_NORMAL, 3, 1, 0);      
}

static void EnemyDoubleScissorPreset(FlyingManager * fm, SDLManager * g, SLList * resourcelist, SLList ** targetlist, SLList ** associatedMissileList)
{
      AddFlying(fm, g, ENEMY, g->windowSize.w, 1* (g->windowSize.h/4), -1, -1, 180, resourcelist, targetlist, associatedMissileList, BEHAVIOR_ENEMY_UP, DESTROYED_OBJECT_BEHAVIOR, -1, 4, ATTACK_NORMAL, 3, 1, 0);
      AddFlying(fm, g, ENEMY, g->windowSize.w, 3* (g->windowSize.h/4), -1, -1, 180, resourcelist, targetlist, associatedMissileList, BEHAVIOR_ENEMY_DOWN, DESTROYED_OBJECT_BEHAVIOR, -1, 4, ATTACK_NORMAL, 3, 1, 0);
}

static void DownCannonStaticPreset(FlyingManager * fm, SDLManager * g, SLList * resourcelist, SLList ** targetlist, SLList ** associatedMissileList)
{
      AddFlying(fm, g, CANNON, g->windowSize.w, 0, -1, -1, 140, resourcelist, targetlist, associatedMissileList, BEHAVIOR_FLY_BY, DESTROYED_OBJECT_BEHAVIOR, -1, 4, ATTACK_NORMAL, 3, 1, 1);
}

static void EnemyFiretrapPreset(FlyingManager * fm, SDLManager * g, SLList * resourcelist, SLList ** targetlist, SLList ** associatedMissileList)
{
      AddFlying(fm, g, FIREMINE, g->windowSize.w, (g->windowSize.h/2), -1, -1, 180, resourcelist, targetlist, associatedMissileList, BEHAVIOR_FLY_BY, DESTROYED_WALL_BEHAVIOR66, 66, 0, ATTACK_NONE, 3, 1, 1);
      AddFlying(fm, g, FIREMINEWALL, g->windowSize.w, (g->windowSize.h/2) - 15, -1, -1, 180, resourcelist, targetlist, associatedMissileList, BEHAVIOR_FLY_BY, DESTROYED_OBJECT_BEHAVIOR, 67, 0, ATTACK_NONE, 3, 1, 1);
      AddFlying(fm, g, FIREMINEWALL, g->windowSize.w, (g->windowSize.h/2) + 15, -1, -1, 180, resourcelist, targetlist, associatedMissileList, BEHAVIOR_FLY_BY, DESTROYED_OBJECT_BEHAVIOR, 68, 0, ATTACK_NONE, 3, 1, 1);
}

static void FirstBossPreset(FlyingManager * fm, SDLManager * g, SLList * resourcelist, SLList ** targetlist, SLList ** associatedMissileList)
{
      AddFlying(fm, g, BOSS, g->windowSize.w, rand () % (g->windowSize.h), -1, -1, 180, resourcelist, targetlist, associatedMissileList, BEHAVIOR_BOSS1, DESTROYED_OBJECT_BEHAVIOR, -1, 1, ATTACK_BOSS1, 100, 1, 0);
}

static void Player1Preset(FlyingManager * fm, SDLManager * g, SLList * resourcelist, SLList ** targetlist, SLList ** associatedMissileList)
{
      AddFlying(fm, g, HERO, 10, (g->windowSize.h/2), -1, -1, 0, resourcelist, targetlist, associatedMissileList, NO_BEHAVIOR, DESTROYED_OBJECT_BEHAVIOR, -1, 4, ATTACK_NORMAL, 3, 2, 0);
}

static void Player2Preset(FlyingManager * fm, SDLManager * g, SLList * resourcelist, SLList ** targetlist, SLList ** associatedMissileList)
{
      AddFlying(fm, g, HERO, 10, 3*(g->windowSize.h/4), -1, -1, 0, resourcelist, targetlist, associatedMissileList, NO_BEHAVIOR, DESTROYED_OBJECT_BEHAVIOR, -1, 4, ATTACK_NORMAL, 3, 2, 0);
}
//================

static void AddFlyingPreset(FlyingManager * fm, SDLManager *g, SLList * resourcelist, SLList ** targetlist, SLList ** associatedMissileList, enum FlyingPreset fp)
{
  if(ENEMY_RAND_UPDN == fp) EnemyRandUpDnPreset(fm, g, resourcelist, targetlist, associatedMissileList);
  if(ENEMY_RAND_UPDN_ROCKET == fp) EnemyRandUpDnRocketPreset(fm, g, resourcelist, targetlist, associatedMissileList);
  if(ENEMY_KAMIKAZE == fp) EnemyKamikazePreset(fm, g, resourcelist, targetlist, associatedMissileList);
  if(ENEMY_DOUBLE_KAMIKAZE == fp) EnemyDoubleKamikazePreset(fm, g, resourcelist, targetlist, associatedMissileList);
  if(ENEMY_ROCKETS_WAVE == fp) EnemyRocketsWavePreset(fm, g, resourcelist, targetlist, associatedMissileList);
  if(ENEMY_WAVE == fp) EnemyWavePreset(fm, g, resourcelist, targetlist, associatedMissileList);
  if(ENEMY_TRIPLE_STATIC == fp) { if(EnemyTripleStaticCheckAvailability(fm, g, resourcelist, targetlist, associatedMissileList)) EnemyTripleStaticPreset(fm, g, resourcelist, targetlist, associatedMissileList); }
  if(ENEMY_TRIPLE_STATIC_SWITCH == fp) { if(EnemyTripleStaticCheckAvailability(fm, g, resourcelist, targetlist, associatedMissileList)) EnemyTripleStaticSwitchPreset(fm, g, resourcelist, targetlist, associatedMissileList); }
  if(ENEMY_DOUBLE_SCISSOR == fp) EnemyDoubleScissorPreset(fm, g, resourcelist, targetlist, associatedMissileList);
  if(DOWN_CANNON_STATIC == fp) DownCannonStaticPreset(fm, g, resourcelist, targetlist, associatedMissileList);
  if(ENEMY_FIRETRAP == fp) EnemyFiretrapPreset(fm, g, resourcelist, targetlist, associatedMissileList);
  if(FIRST_BOSS == fp) FirstBossPreset(fm, g, resourcelist, targetlist, associatedMissileList);
  if(PLAYER1 == fp) Player1Preset(fm, g, resourcelist, targetlist, associatedMissileList);
  if(PLAYER2 == fp) Player2Preset(fm, g, resourcelist, targetlist, associatedMissileList);
}
  

static void PrintFlyingList(SLList * list)
{
  while(list && list->data) {
    printf("[%d]", ((Flying*)list->data)->id);
    if(list->next)
      printf("->");
    else
      printf("/\n");
    list = list->next;
  }
}

static void HandleFlying(SDLManager *g, SLList * iterator)
{
  Flying * current = ((Flying *)iterator->data);
  if(current->behavior != NO_BEHAVIOR) BehaviorSelect(current->behavior, g, iterator);
  if(current->weaponTimeout) current->weaponTimeout--;
  if(current->immunity) current->immunity--;
  if(!current->immunity && // if we have immunity that means the player is respawning
                           // do not destroy him then ...please
     (current->o.position.x < -current->o.position.w ||
      current->o.position.y < -current->o.position.h ||
      current->o.position.y > g->windowSize.h)) {
    printf("bye Flying!\n");
    iterator->markForDeletion = 1;
    PrintFlyingList((void *)iterator);
  }
}

/* UNUSED PIECE OF CODE
static void DrawHealthWithFlying()
{
      if(current->health > 0) {
      int health = current->health;
      SDL_Rect shieldplace;
      (current->o.position.y > TOP_OFFSET + 10) ? (shieldplace.y = current->o.position.y - 10) : (shieldplace.y = current->o.position.y + current->o.position.w);
      shieldplace.w = 5;
      shieldplace.h = 5;
      for(int i = 0; i < health; i++) {
      shieldplace.x = current->o.position.x + (i * (10));
      if(shieldplace.x > current->o.position.w + current->o.position.x) {
      shieldplace.y -= 10; shieldplace.x = current->o.position.x; health -= i; i = 0;
      }
      SDL_RenderCopy(s->g->renderer, ((Resource *)GetNthElement(s->t->chapteronetextures, SHIELD_RES)->data)->texture, NULL, &shieldplace);
      }
      }
}
*/

static void DrawFlying(SDLManager *g, SLList * iterator)
{
  Flying * current = ((Flying *)iterator->data);
  if(current->o.position.y < TOP_OFFSET) current->o.position.y = TOP_OFFSET;
  if(current->o.position.y > g->windowSize.h - current->o.position.h) current->o.position.y = g->windowSize.h - current->o.position.h;
  SDL_Rect currentframe = *((SDL_Rect*)current->o.currentFrame->data);
  if(current->ishit) {
    current->ishit--;
    SDL_SetTextureColorMod(((Resource *)current->o.resource)->texture, 255, 100, 255);
  } else {
    SDL_SetTextureColorMod(((Resource *)current->o.resource)->texture, 255, 255, 255);
  }
  if(current->immunity % 2 == 0)
    SDL_RenderCopy(g->renderer, ((Resource *)current->o.resource)->texture, &currentframe, &current->o.position);
}

static SLList * GetFlyingByCodename(SLList * list, int codename)
{
  SLList * iterator;
  for(iterator = list; iterator; iterator = iterator->next) {
    if(iterator->dataType == FLYING_T && // only Flying types have Codenames
       ((Flying*)iterator->data)->codename == codename)
    {
      return (iterator);
    }
  }
  return NULL;
}


void InitFlyingManager(FlyingManager * fm, MissileManager * mm)
{
  fm->mm = mm;
  //----
  fm->AddHitbox = &AddFlyingHitbox;
  fm->AddPreset = &AddFlyingPreset;
  fm->Add = &AddFlying;
  fm->Handle = &HandleFlying;
  fm->Draw = &DrawFlying;
  fm->Print = &PrintFlyingList;
  fm->Get = &GetFlyingByCodename;
}  

