/*
    This file is a part of cargo - a space shooter game
    Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _FLYINGMANAGER_H_
#define _FLYINGMANAGER_H_

#include "CommonManager.h"
#include "SLList.h"
#include "MissileManager.h"

enum AttackType {
  ATTACK_NONE,
  ATTACK_NORMAL,
  ATTACK_DOUBLE,
  ATTACK_TRIPLE,
  ATTACK_BOSS1,
  ATTACK_ROCKET,
};

enum BehaviorScheme {
  NO_BEHAVIOR,
  BEHAVIOR_HERO,
  BEHAVIOR_WRECKAGE,
  BEHAVIOR_GATE,
  BEHAVIOR_FLY_BY,
  BEHAVIOR_ENEMY3,
  BEHAVIOR_ENEMY2,
  BEHAVIOR_ENEMY1,
  BEHAVIOR_ENEMY_WAVE,
  BEHAVIOR_ENEMY_UP,
  BEHAVIOR_ENEMY_DOWN,
  BEHAVIOR_BOSS1,
  DESTROYED_OBJECT_BEHAVIOR,
  AFTER_DEATH87,
  DESTROYED_WALL_BEHAVIOR66,
  COUNT_BEHAVIORS,
};

enum FlyingType {
  ENEMY,
  CANNON,
  FIREMINE,
  FIREMINEWALL,
  BOSS,
  HERO,
};

enum FlyingPreset {
  ENEMY_RAND_UPDN,
  ENEMY_RAND_UPDN_ROCKET,
  ENEMY_KAMIKAZE,
  ENEMY_DOUBLE_KAMIKAZE,
  ENEMY_ROCKETS_WAVE,
  ENEMY_WAVE,
  ENEMY_TRIPLE_STATIC,
  ENEMY_TRIPLE_STATIC_SWITCH,
  ENEMY_DOUBLE_SCISSOR,
  DOWN_CANNON_STATIC,
  ENEMY_FIRETRAP,
  FIRST_BOSS,
  PLAYER1,
  PLAYER2,
};

struct _FlyingManager; // declaration

typedef struct _Flying {
  int id; // Id - for debugging
  int codename; // this is so that we can find the object we are looking for.
                // Not all objects have to have this set, only the ones we want to find later.
                // Codename must be uniqe and that is the role of the programmer to keep it right
                // I (said programmer) can't be bothered writing a mechanism to guard that shit now.
                // There are more serious issues at hand and this whole mchanism has to be rewritten anyway.
  Core o;
  SDL_Point desiredPosition; // position this object would like to reach, used by behavior
  char health; // health
  char lives; // lives
  char leaveWreckage;
  char ishit; // object just got hit - used to signal (by a flash) that the object got hit and damaged)
  char immunity; // during respawn
  double angle; // where is this thing headed
  char weaponTimeout; // some timeout to not be able to shoot infinitely fast
  char weaponReloadTime; // in furure whole type - lasers, blasters, all that shit, now just reload time used with weapon timeout
  enum AttackType attackType; //
  enum BehaviorScheme behavior; //this pick governs "AI" of the object
  enum BehaviorScheme behaviorAfterDeath; //this pick governs the object after its destruction

  // related
  SLList ** listhead; // list head for this object
  SLList ** associatedMissileList; // missile list where objects stores its missiles
  struct _FlyingManager * fm; // pointer to itself i guess
} Flying;

typedef struct _FlyingManager {
  MissileManager * mm; // shoot (i.e. add missiles) using this manager
  
  void (*AddHitbox)(Flying **new, int hitboxnum, ...);
  void (*AddPreset)(struct _FlyingManager * fm, SDLManager *g, SLList * resourcelist, SLList ** targetlist, SLList ** associatedMissileList, enum FlyingPreset fp);

  void (*Add)(struct _FlyingManager * fm, SDLManager * g, enum FlyingType ft, int startx, int starty,
              int desx, int desy, double angle, SLList * resourcelist, SLList ** targetlist,
              SLList ** associatedMissileList, enum BehaviorScheme behavior,
              enum BehaviorScheme behaviorAfterDeath, int codename,
              char weaponReloadTime, enum AttackType attackType,
              char health, char lives, char leaveWrekage);
  void (*Handle)(SDLManager *g, SLList * iterator);
  void (*Draw)(SDLManager *g, SLList * iterator);
  void (*Print)(SLList *);
  SLList * (* Get)(SLList * list, int codename);
} FlyingManager;

void InitFlyingManager(FlyingManager * fm, MissileManager * mm);

#endif // _FLYINGMANAGER_H_

