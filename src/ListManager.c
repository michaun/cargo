/*
  This file is a part of cargo - a space shooter game
  Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//==============================================================================
#include "ListManager.h"

//==============================================================================

void ListManagerInit(ListManager * t);

//==============================================================================

static int ListManagerExecute(ListManager * lm);
static void ListManagerExit(ListManager * lm);

//==============================================================================

//==============================================================================

static int ListManagerExecute(ListManager * lm)
{
  return 0; //success
}

//==============================================================================
static void ListManagerExit(ListManager * lm)
{
  return;
}

//==============================================================================
void ListManagerInit(ListManager * lm)
{
  lm->titleTextures = NULL;
  
  lm->scrollingTextures = NULL;
  
  lm->chapterOneTextures = NULL;
  lm->chapterOneExplosions = NULL;
  lm->chapterOneHeroes = NULL;
  lm->chapterOneMissiles = NULL;
  lm->chapterOneEnemies = NULL;
  lm->chapterOneEnemyMissiles = NULL;
  lm->chapterOnePickables = NULL;
  lm->chapterOneObjectives = NULL;
  lm->chapterOneBosses = NULL;

  lm->OnExecute = &ListManagerExecute;
  lm->OnExit = &ListManagerExit;
}

