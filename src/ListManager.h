/*
    This file is a part of cargo - a space shooter game
    Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _LISTMANAGER_H_
#define _LISTMANAGER_H_

#include "SLList.h"
#include "ListManager.h"

//==============================================================================

typedef struct _ListManager {

  SLList * titleTextures;
  
  SLList * scrollingTextures;

  SLList * chapterOneTextures; // Texture
  SLList * chapterOneExplosions; // Explosion
  SLList * chapterOneEnemies;
  SLList * chapterOneBosses;
  SLList * chapterOneEnemyMissiles; //Missile
  SLList * chapterOneMissiles; //Missile
  SLList * chapterOneHeroes; //Hero
  SLList * chapterOnePickables;
  SLList * chapterOneObjectives;

  int (*OnExecute)(struct _ListManager *);
  void (*OnExit)(struct _ListManager *);

} ListManager;

//==============================================================================
void ListManagerInit(ListManager *lm);

// !!!!!

#endif // _LISTMANAGER_H_
