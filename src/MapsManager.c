/*
  This file is a part of cargo - a space shooter game
  Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//==============================================================================
#include "ListManager.h"
#include "MapsManager.h"

//==============================================================================
//==============================================================================
//==============================================================================
static void TestMap(MapsManager *mam, SDLManager *g, int currentDistance)
{
  switch(currentDistance) {
  case 100:
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_TRIPLE_STATIC);
    break;
  case 200:
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_TRIPLE_STATIC);
    ///    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_FIRETRAP);
    ///mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_FIRETRAP);
    ///mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_FIRETRAP);

    mam->pm->Add(mam->pm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOnePickables, WEAPON_TURBO);
    break;
  }
}

static void Level1Map(MapsManager *mam, SDLManager *g, int currentDistance)
{
  switch(currentDistance) {
  case 100:
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_ROCKETS_WAVE);
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_ROCKETS_WAVE);
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_ROCKETS_WAVE);
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, DOWN_CANNON_STATIC);
    break;
  case 300:
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN);
    break;
  case 400:
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_FIRETRAP);
    break;
  case 600:
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_DOUBLE_SCISSOR);
    break;
  case 1000:
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN);
    break;
  case 1300:
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_DOUBLE_KAMIKAZE);
    break;
  case 1500:
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_TRIPLE_STATIC_SWITCH);
    break;
  case 1700:
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_FIRETRAP);
    break;
  case 1900:
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_DOUBLE_SCISSOR);
    break;
  case 2000:
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN);
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN);
    break;
  case 3000:
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN_ROCKET);
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN_ROCKET);
    break;
  case 3100:
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN);
    break;
  case 4000:
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneBosses, &mam->lm->chapterOneEnemyMissiles, FIRST_BOSS);
    break;
  }
  if(rand() % 100 < 1)
    mam->pm->Add(mam->pm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOnePickables, rand() % NUM_PICKABLES);
}

static void Level2Map(MapsManager *mam, SDLManager *g, int currentDistance)
{
  switch(currentDistance) {
  case 100:
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN);
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN);
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN);
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN);
    mam->pm->Add(mam->pm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOnePickables, WEAPON_DOUBLE);
    break;
  case 400:
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_DOUBLE_KAMIKAZE);
    break;
  case 500:
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN);
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN);
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN);
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN);
    mam->pm->Add(mam->pm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOnePickables, WEAPON_TRIPLE);
    break;
  case 600:
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN);
    mam->pm->Add(mam->pm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOnePickables, WEAPON_TURBO);
    mam->pm->Add(mam->pm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOnePickables, HEALTH);
    mam->pm->Add(mam->pm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOnePickables, HEALTH);
    break;
  case 800:
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN);
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN);
mam->pm->Add(mam->pm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOnePickables, HEALTH);
mam->pm->Add(mam->pm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOnePickables, LIFE);
    break;
  case 1000:
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN);
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN);
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN);
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN);
mam->pm->Add(mam->pm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOnePickables, HEALTH);
mam->pm->Add(mam->pm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOnePickables, WEAPON_TRIPLE);
    break;
  case 1400:
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN);
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN);
mam->pm->Add(mam->pm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOnePickables, LIFE);
    break; 
  case 2000:
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN);
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN);
mam->pm->Add(mam->pm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOnePickables, HEALTH);
    break;
  case 2400:
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneBosses, &mam->lm->chapterOneEnemyMissiles, FIRST_BOSS);
mam->pm->Add(mam->pm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOnePickables, HEALTH);
    break;
  }
}

static void GenerateRandomMap(MapsManager *mam, SDLManager *g, int currentDistance)
{
  int bossDistance = 15000;
  if(rand() % 80 < 5 &&
     CountList(mam->lm->chapterOneEnemies) < 12 &&
     currentDistance < bossDistance) {
    switch(rand() % 7) {
    case 0:
      mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_RAND_UPDN);
      break;
    case 1:
      mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_DOUBLE_SCISSOR);
      break;
    case 2:
      mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_ROCKETS_WAVE);
      break;
    case 3:
      mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, DOWN_CANNON_STATIC);
      break;
    case 4:
      mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, DOWN_CANNON_STATIC);
      break;
    case 5:
      mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_DOUBLE_KAMIKAZE);
      break;
    case 6:
      mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneEnemies, &mam->lm->chapterOneEnemyMissiles, ENEMY_TRIPLE_STATIC);
      break;
    }
  }
    
  if(currentDistance == bossDistance)
    mam->fm->AddPreset(mam->fm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOneBosses, &mam->lm->chapterOneEnemyMissiles, FIRST_BOSS);
  
  if(rand() % 1000 < 2 && CountList(mam->lm->chapterOnePickables) < 3) {
    mam->pm->Add(mam->pm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOnePickables, HEALTH); }
  if(rand() % 1000 < 2 && CountList(mam->lm->chapterOnePickables) < 3) {
    mam->pm->Add(mam->pm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOnePickables, LIFE); }
  if(rand() % 1000 < 2 && CountList(mam->lm->chapterOnePickables) < 3) {
    mam->pm->Add(mam->pm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOnePickables, WEAPON_TRIPLE); }
  if(rand() % 1000 < 2 && CountList(mam->lm->chapterOnePickables) < 3) {
    mam->pm->Add(mam->pm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOnePickables, WEAPON_DOUBLE); }
  if(rand() % 1000 < 2 && CountList(mam->lm->chapterOnePickables) < 3) {
    mam->pm->Add(mam->pm, g, mam->lm->chapterOneTextures, &mam->lm->chapterOnePickables, WEAPON_TURBO); }
}

static void (*MapsManagerReturnMap(enum MapId id))(MapsManager *mam, SDLManager *g, int currentDistance)
{
  switch(id) {
  case TEST_MAP:
    return TestMap;
  case LEVEL1_MAP:
    return Level1Map;
  case LEVEL2_MAP:
    return Level2Map;
  case RANDOMIZED_MAP:
    return GenerateRandomMap;
  }
  return NULL;
}      

//==============================================================================
static int MapsManagerExecute(MapsManager * mam)
{  
  return 0; //success
}

//==============================================================================
static void MapsManagerExit(MapsManager * mam)
{
  // nothing to do
}
//==============================================================================
void MapsManagerInit(MapsManager * mam, FlyingManager * fm, PickableManager * pm, ListManager * lm)
{
  mam->fm = fm;
  mam->pm = pm;
  mam->lm = lm;

  mam->OnExecute = &MapsManagerExecute;
  mam->OnExit = &MapsManagerExit;
  mam->ReturnMap = &MapsManagerReturnMap;
}
