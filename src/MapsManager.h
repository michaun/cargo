/*
    This file is a part of cargo - a space shooter game
    Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _MAPSMANAGER_H_
    #define _MAPSMANAGER_H_

#include "ListManager.h"
#include "FlyingManager.h"
#include "PickableManager.h"

//==============================================================================
enum MapId {
  TEST_MAP,
  LEVEL1_MAP,
  LEVEL2_MAP,
  RANDOMIZED_MAP,
};

typedef struct _MapsManager {
  FlyingManager * fm;
  PickableManager * pm;
  ListManager * lm;
  struct _MapsManager * mm;

  int (*OnExecute)(struct _MapsManager *);
  void (*OnExit)(struct _MapsManager *);
  // hold ya pants now!
  // MapSelector is a pointer to a function that returns a pointer to a function
  void (*(*ReturnMap)(enum MapId))(struct _MapsManager *mam, SDLManager *g, int currentDistance);
} MapsManager;

//==============================================================================
void MapsManagerInit(MapsManager * mam, FlyingManager * fm, PickableManager * pm, ListManager * lm);

//==============================================================================

#endif
