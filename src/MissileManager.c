/*
    This file is a part of cargo - a space shooter game
    Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//==============================================================================
#include "MissileManager.h"

static void AddMissile(MissileManager * mm, SLList *resourcelist, SLList *who, double angle, char velocity, char ignitionTime, char ignitionRate, char ignitionFuel, SLList **targetlist, SDL_Point position, enum MissileTypeEnum missileType)
{
  ///
  int missilew;
  int missileh;
  Resource * res;
  switch(missileType) {
  case ROCKET:
    res = (Resource *)GetNthElement(resourcelist, ROCKET_RES)->data;
    missilew = ((SDL_Rect *)res->clip->data)->w;
    missileh = ((SDL_Rect *)res->clip->data)->h;
    break;
  case MISSILE:
  default:    
    res = (Resource *)GetNthElement(resourcelist, MISSILE_RES)->data;
    missilew = ((SDL_Rect *)res->clip->data)->w;
    missileh = ((SDL_Rect *)res->clip->data)->h;
    //Mix_PlayChannel(-1, t->missilesound, 0);
    break;
  }
  Missile * current = malloc(sizeof(Missile));
  current->owner = who;
  current->type = missileType;
  current->angle = angle;
  current->velocity = velocity;
  current->ignitionTime = ignitionTime;
  current->ignitionRate = ignitionRate;
  current->ignitionFuel = ignitionFuel;
  current->o.position.x = position.x;
  current->o.position.y = position.y;
  current->o.position.w = missilew;
  current->o.position.h = missileh;
  current->listhead = targetlist;
  current->mm = mm;
  current->o.resource = res;
  current->o.currentFrame = current->o.resource->clip; // first frame should be here
  current->o.hitbox = NULL;
  AddHitBoxR(&current->o.hitbox, &current->o.position, 1);
  AddToList(targetlist, current, sizeof(Missile), MISSILE_T, DeleteInternalCore);
  free(current);
}

static void HandleMissile(SDLManager *g, SLList * iterator)
{
  ///
  Missile * current = ((Missile *)iterator->data);
  int missilew = ((SDL_Rect *)((Resource *)GetNthElement(*(current->o.resource->listhead), MISSILE_RES)->data)->clip->data)->w;
  switch(current->type) {
  case ROCKET:
    if(current->ignitionTime) {
      current->ignitionTime--;
      current->o.position.y += 5; // freefall
    } else {
      if(current->ignitionFuel) {
        current->ignitionFuel--;
        current->velocity += current->ignitionRate;
        current->o.position.y += 5; // welp, continue freefall
      }
    }
    /* no break; */
  case MISSILE:
  default:
    current->o.position.x += (current->velocity * cos(current->angle*M_PI/180));
    current->o.position.y += (current->velocity * sin(current->angle*M_PI/180));
    break;
  }
  // check boundaries
  if(current->o.position.x < -missilew) iterator->markForDeletion = 1;
  if(current->o.position.x > g->windowSize.w) iterator->markForDeletion = 1;
}  

static void DrawMissiles(SDLManager *g, SLList * iterator)
{
  Missile * current = ((Missile *)iterator->data);
  switch(current->type) {
  case ROCKET:
    SDL_RenderCopyEx(g->renderer, ((Resource *)GetNthElement(*(current->o.resource->listhead), ROCKET_RES)->data)->texture,
                     NULL, &current->o.position, current->angle, 0, SDL_FLIP_NONE);
    break;
  case MISSILE:
  default:
    SDL_RenderCopyEx(g->renderer, ((Resource *)GetNthElement(*(current->o.resource->listhead), MISSILE_RES)->data)->texture,
                     NULL, &current->o.position, current->angle, 0, SDL_FLIP_NONE);
    break;
  }
}

void InitMissileManager(MissileManager * mm)
{
  mm->Add = &AddMissile;
  mm->Draw = &DrawMissiles;
  mm->Handle = &HandleMissile;
}
