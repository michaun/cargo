/*
    This file is a part of cargo - a space shooter game
    Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _MISSILEMANAGER_H_
#define _MISSILEMANAGER_H_

#include "CommonManager.h"
#include "SLList.h"

enum MissileTypeEnum {
  MISSILE,
  ROCKET,
};

struct _MissileManager; // declaration

typedef struct _Missile {
  Core o;
  enum MissileTypeEnum type;
  double angle;
  char velocity;
  char ignitionTime; // after what time ignition launches
  char ignitionRate; // what is the speedingrate
  char ignitionFuel; // how much ignition
  // for example: after 2 seconds (ignitionTime), ignition launches and missile velocity
  // increases by 5 (ignitionRate), up to 30 (ignitionFuel)
  
  // related
  SLList * owner; // owner of the missile - this is A Flying type list
  SLList ** listhead;
  struct _MissileManager * mm; // pointer to itself
} Missile;

typedef struct _MissileManager {
  void (*Add)(struct _MissileManager * mm, SLList *resourcelist, SLList *who, double angle, char velocity, char ignitionTime, char ignitionRate, char ignitionFuel, SLList **targetlist, SDL_Point position, enum MissileTypeEnum missileType);
  void (*Handle)(SDLManager *g, SLList * iterator);
  void (*Draw)(SDLManager *g, SLList * iterator);
} MissileManager;

void InitMissileManager(MissileManager * mm);
 
#endif //_MISSILEMANAGER_H_

