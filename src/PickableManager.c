/*
    This file is a part of cargo - a space shooter game
    Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//==============================================================================
#include "PickableManager.h"

static void AddPickableXY(PickableManager * pm, SDLManager *g, SLList * resourcelist, SLList ** targetlist, enum WhatIsIt what, int x, int y)
{
  printf("Add Pickable!\n");
  Pickable * new = malloc(sizeof(Pickable));
  int pickablew, pickableh;
  switch(what) {
  default:
  case HEALTH:
    new->o.resource = ((Resource *)GetNthElement(resourcelist, SHIELD_RES)->data); // shield
    SDL_QueryTexture(new->o.resource->texture, NULL, NULL, &pickablew, &pickableh);
    break;
  case WEAPON_DOUBLE: // we can do them in one go since crates are of the same size
  case WEAPON_TRIPLE:
  case WEAPON_TURBO:
    new->o.resource = ((Resource *)GetNthElement(resourcelist, CRATE_RES)->data); // crate
    pickablew = ((SDL_Rect *) GetNthElement(new->o.resource->clip, 0)->data)->w;
    pickableh = ((SDL_Rect *) GetNthElement(new->o.resource->clip, 0)->data)->h;
    break;
  case LIFE:
    new->o.resource = ((Resource *)GetNthElement(resourcelist, SHIPICON_RES)->data); // shipicon
    SDL_QueryTexture(new->o.resource->texture, NULL, NULL, &pickablew, &pickableh);
    break;
  }
  new->o.position.w = pickablew; // this actually doesn't make sense for crates now
  new->o.position.h = pickableh;
  new->o.position.y = y;
  new->o.position.x = x;
  if(new->o.position.y >= g->windowSize.h) new->o.position.y = (g->windowSize.h - new->o.position.h);
  if(new->o.position.y < TOP_OFFSET) new->o.position.y = TOP_OFFSET;

  new->pm = pm;
  new->listhead = targetlist;
  
  new->o.hitbox = NULL;
  new->what = what;
  AddHitBoxR(&new->o.hitbox, &new->o.position, 1);
  // list things
  AddToList(targetlist, (void*)new, sizeof(Pickable), PICKABLE_T, DeleteInternalCore);
  free(new); //add to list makes a copy of the element
}

static void AddPickable(PickableManager * pm, SDLManager *g, SLList * resourcelist, SLList ** targetlist, enum WhatIsIt what)
{
  AddPickableXY(pm, g, resourcelist, targetlist, what, g->windowSize.w, rand() % g->windowSize.h);                                                         // y may go out of bounds
                                                                                       // but AddPickableXY will fix that
}



static void HandlePickables(SDLManager *g, SLList * iterator)
{
  Pickable * current = ((Pickable *)iterator->data);
  if(current->o.position.y < TOP_OFFSET) current->o.position.y = TOP_OFFSET;
  if(current->o.position.y > g->windowSize.h - current->o.position.h) current->o.position.y = g->windowSize.h - current->o.position.h;
  current->o.position.x -= 5;
  if(current->o.position.x < -current->o.position.w ||
     current->o.position.y < -current->o.position.h ||
     current->o.position.y > g->windowSize.h) {
    printf("bye pickable!\n");
    iterator->markForDeletion = 1;
  }
}

static void DrawPickable(SDLManager *g, SLList * iterator)
{
  Pickable * current = ((Pickable *)iterator->data);
  SDL_Rect * clip;
  switch(current->what) {
  case WEAPON_DOUBLE:
    clip = ((SDL_Rect *) GetNthElement(current->o.resource->clip, 0)->data);
    break;
  case WEAPON_TRIPLE:
    clip = ((SDL_Rect *) GetNthElement(current->o.resource->clip, 1)->data);
    break;
  case WEAPON_TURBO:
    clip = ((SDL_Rect *) GetNthElement(current->o.resource->clip, 2)->data);
    break;
  default:
    clip = NULL;
    break;
  }
  SDL_RenderCopy(g->renderer, current->o.resource->texture, clip, &current->o.position);
}

void InitPickableManager(PickableManager * pm)
{
  pm->AddXY = &AddPickableXY;
  pm->Add = &AddPickable;
  pm->Draw = &DrawPickable;
  pm->Handle = &HandlePickables;
}
