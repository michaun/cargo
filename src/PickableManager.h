/*
    This file is a part of cargo - a space shooter game
    Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _PICKABLEMANAGER_H_
#define _PICKABLEMANAGER_H_

#include "CommonManager.h"

enum WhatIsIt {

  HEALTH,
  WEAPON_DOUBLE,
  WEAPON_TRIPLE,
  WEAPON_TURBO,
  LIFE,
  NUM_PICKABLES,
};

struct _PickableManager;

typedef struct _Pickable {
  Core o;
  enum WhatIsIt what;
  SLList ** listhead;
  struct _PickableManager * pm;
} Pickable;

typedef struct _PickableManager {
  void (*AddXY)(struct _PickableManager * pm, SDLManager *g, SLList * resourcelist, SLList ** targetlist, enum WhatIsIt what, int x, int y);
  void (*Add)(struct _PickableManager * pm, SDLManager *g, SLList * resourcelist, SLList ** targetlist, enum WhatIsIt what);
  void (*Handle)(SDLManager *g, SLList * iterator);
  void (*Draw)(SDLManager *g, SLList * iterator);
} PickableManager;

void InitPickableManager(PickableManager * pm);

#endif //_PICKABLEMANAGER_H_
