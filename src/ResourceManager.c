/*
    This file is a part of cargo - a space shooter game
    Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//==============================================================================
#include "SLList.h"
#include "CommonManager.h" // SDL_RECT_T
#include "ResourceManager.h"
#include "CairoArt.h" // CreateTextTexture

//==============================================================================
static void DeleteInternalAnimated(SLList * element)
{
  SLList * iterator;
  void * data = element->data;
  while((iterator = ((Resource *)data)->clip) != NULL) DeleteFromList(&((Resource*)data)->clip, iterator);
  SDL_DestroyTexture(((Resource*)data)->texture);
}

//==============================================================================
static void AddResourceToList(ResourceManager * rm, SLList ** list, SDL_Texture * res, int clipnum, ...)
{
  Resource * anim = malloc(sizeof(Resource));
  anim->texture = res;
  anim->rm = rm;
  anim->listhead = list;
  anim->clip = NULL;
  if(clipnum == 0) {
    int w, h;
    SDL_QueryTexture(res, NULL, NULL, &w, &h);
    SDL_Rect size = {0, 0, w, h};
    AddToBackList(&anim->clip, &size, sizeof(SDL_Rect), SDL_RECT_T, NULL);
  } else {
    SDL_Rect r;
    va_list vl;
    va_start(vl, clipnum);
    for(int i = 0; i < clipnum; i++) {
      r.x = va_arg(vl, int);
      r.y = va_arg(vl, int);
      r.w = va_arg(vl, int);
      r.h = va_arg(vl, int);
      printf("framezzz: %d %d %d %d\n", r.x, r.y, r.w, r.h);
      AddToBackList(&anim->clip, &r, sizeof(SDL_Rect), SDL_RECT_T, NULL);
    }
    va_end(vl);
    printf("done\n");
  }
  
  AddToBackList(list, anim, sizeof(Resource), RESOURCE_T, DeleteInternalAnimated);
  free(anim);
}

static void AddTextsToList(ResourceManager * rm, SDLManager *g, SLList ** list, char ** texts)
{
  SDL_Texture * helper;

  for (int i = 0; texts[i] != NULL; i++) {
    helper = CreateTextTexture(g, texts[i], g->windowSize.w, 30);
    AddResourceToList(rm, list, helper, 0);
  }
}

//==============================================================================
static int ResourceManagerExecute(ResourceManager * g)
{  
  return 0; //success
}

//==============================================================================
static void ResourceManagerExit(ResourceManager * g)
{
  // nothing to do
}
//==============================================================================
void ResourceManagerInit(ResourceManager * g)
{
  g->OnExecute = &ResourceManagerExecute;
  g->OnExit = &ResourceManagerExit;
  g->AddResourceToList = &AddResourceToList;
  g->AddTextsToList = &AddTextsToList;
}
