/*
    This file is a part of cargo - a space shooter game
    Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _RESOURCEMANAGER_H_
    #define _RESOURCEMANAGER_H_

#include "SLList.h"
#include "SDLManager.h"

//==============================================================================
enum RESOURCE_ID {
  SHIP_RES, //0
  SPACE_RES,
  MISSILE_RES,
  SHIELD_RES,
  ENEMY_RES,
  BOSS_RES,
  CRATE_RES,
  SHIPICON_RES,
  ROCKET_RES,
  CANNON_RES,
  TOPBAR_RES,
  FIREMINE_RES,
  FIREWALL_RES,
  EXPLOSION_RES,
};

typedef struct _Resource {
  SDL_Texture * texture;
  SLList * clip; // SDL_Rect
  SLList ** listhead;
  struct _ResourceManager * rm;
} Resource;

typedef struct _ResourceManager {

  int (*OnExecute)(struct _ResourceManager *);
  void (*OnExit)(struct _ResourceManager *);
  void (*AddResourceToList)(struct _ResourceManager * rm, SLList ** list, SDL_Texture * res, int clipnum, ...);
  void (*AddTextsToList)(struct _ResourceManager * rm, SDLManager *g, SLList ** list, char ** texts);
} ResourceManager;

//==============================================================================
void ResourceManagerInit(ResourceManager *g);

//==============================================================================

#endif
