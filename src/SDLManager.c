/*
    This file is a part of cargo - a space shooter game
    Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//==============================================================================
#include "SDLManager.h"

#include <SDL2/SDL_mixer.h>

//==============================================================================
static void UpdateWindowSize(SDLManager * g)
{
  g->windowSize.x = 0; g->windowSize.y = 0;
  SDL_GetWindowSize(g->window, &g->windowSize.w, &g->windowSize.h);
}

static void SetWindowSize(SDLManager * g, int width, int height)
{
  SDL_SetWindowSize(g->window, width, height);
  UpdateWindowSize(g);
}

//==============================================================================
static int SDLManagerExecute(SDLManager * g)
{  
  if(SDL_Init(SDL_INIT_EVERYTHING) < 0) {
    return -1;
  }

  //Initialize SDL_mixer
  if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024 ) < 0 ) {
    printf( "SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError() );
    return -1;
  }

  g->window = SDL_CreateWindow("Cargo", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, SDL_WINDOW_SHOWN);
  UpdateWindowSize(g);
  g->renderer = SDL_CreateRenderer(g->window, -1, SDL_RENDERER_ACCELERATED);

  return 0; //success
}

//==============================================================================
static void SDLManagerExit(SDLManager * g)
{
  Mix_CloseAudio();
  SDL_DestroyRenderer(g->renderer);
  SDL_DestroyWindow(g->window);
  SDL_Quit();
}
//==============================================================================
void SDLManagerInit(SDLManager * g)
{
  g->OnExecute = &SDLManagerExecute;
  g->OnExit = &SDLManagerExit;
  g->UpdateWindowSize = &UpdateWindowSize;
  g->SetWindowSize = &SetWindowSize;
}
