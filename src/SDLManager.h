/*
    This file is a part of cargo - a space shooter game
    Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _SDLMANAGER_H_
    #define _SDLMANAGER_H_

#include <SDL2/SDL.h>

#define TOP_OFFSET 30
#define FLYING_SPEED 10 //this is the game's scroll speed
#define SHIP_SPEED 15 // this is how fast the ship can go


//==============================================================================
typedef struct _SDLManager {
  SDL_Event event;
  SDL_Window * window;
  SDL_Rect windowSize;
  SDL_Renderer * renderer;
  SDL_Rect camera;
  
  int (*OnExecute)(struct _SDLManager *);
  void (*OnExit)(struct _SDLManager *);
  void (*UpdateWindowSize)(struct _SDLManager * g);
  void (*SetWindowSize)(struct _SDLManager * g, int width, int height);
} SDLManager;

//==============================================================================
void SDLManagerInit(SDLManager *g);

//==============================================================================

#endif
