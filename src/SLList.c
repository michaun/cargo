/*
    This file is a part of cargo - a space shooter game
    Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//==============================================================================
#include "SLList.h"

//==============================================================================
void AddToList(SLList ** head, void * data, int dataSize, char dataType, void (*cleanup)(SLList*))
{
  SLList * new = malloc(sizeof(SLList));
  new->data = malloc(dataSize);
  memcpy(new->data, data, dataSize);
  new->dataType = dataType;
  new->cleanup = cleanup;
  new->next = *head; //head of the list to the back
  new->markForDeletion = 0;
  *head = new; // new element to the front
}

void AddToBackList(SLList ** head, void * data, int dataSize, char dataType, void (*cleanup)(SLList*))
{
  SLList * hptr = *head;
  SLList * new = malloc(sizeof(SLList));
  new->data = malloc(dataSize);
  memcpy(new->data, data, dataSize);
  new->dataType = dataType;
  new->cleanup = cleanup;
  new->next = NULL;
  new->markForDeletion = 0;
  if(!hptr) { *head = new; }
  else {
    while(hptr && hptr->next) hptr = hptr->next;
    hptr->next = new;
  }
}

void DeleteFromList(SLList ** elist, SLList * e)
{
  SLList * current = *elist;
  SLList * tmp;
  if(current == e) {
    tmp = current->next;
    if(current->cleanup) current->cleanup(current);
    free(current->data);
    current->data = NULL;
    free(current);
    current = NULL;
    *elist = tmp;
    return;
  }
  while(current->next) {
    if(current->next == e) {
      tmp = current->next->next;
      if(current->cleanup) current->cleanup(current->next);
      free(current->next->data);
      current->next->data = NULL;
      free(current->next);
      current->next = NULL;
      current->next = tmp;
      return;
    }
    current = current->next;
  }
}

int CountList(SLList * elist)
{
  int i = 0;
  for(SLList * current = elist; current; current = current->next) i++;
  return i;
}

SLList * GetNthElement(SLList * list, int n)
{
  while(list && n--) list = list->next;
  return list;
}
