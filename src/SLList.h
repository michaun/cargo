/*
    This file is a part of cargo - a space shooter game
    Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _SLLIST_H_
    #define _SLLIST_H_

#include <stdlib.h> //malloc
#include <string.h> //memcpy
//==============================================================================
typedef struct _SLList {
  void * data;
  void (*cleanup)(struct _SLList *);
  char markForDeletion;
  char dataType; // type of this data element
  struct _SLList * next;
} SLList;


void AddToList(SLList ** head, void * data, int dataSize, char dataType, void (*cleanup)(SLList*));
void AddToBackList(SLList ** head, void * data, int dataSize, char dataType, void (*cleanup)(SLList*));
void DeleteFromList(SLList ** elist, SLList * e);
int CountList(SLList * elist);
SLList * GetNthElement(SLList * list, int n);


#endif /*_SLLIST_H_*/
