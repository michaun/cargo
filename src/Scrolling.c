/*
    This file is a part of cargo - a space shooter game
    Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//==============================================================================
#include "Scrolling.h"
#include "ControlManager.h"
#include "ResourceManager.h"

#include "ListManager.h"

//==============================================================================
static void DrawScrollingAnimation(SDLManager * g, ControlManager * cm, ListManager * lm)
{
  //char AudioChannel = 0;
  int CreditsPos = 0; //filled later
  int Scrolling = 0; //filled later
  unsigned int Timer = 0;
  int w,h;
  SDL_QueryTexture(((Resource *)GetNthElement(lm->scrollingTextures, 0)->data)->texture, NULL, NULL, &w, &h);  
  CreditsPos = g->windowSize.h;
  Scrolling = 0 - (h+5)*(CountList(lm->scrollingTextures)+1); // some slack at the end

  while(CreditsPos != Scrolling) {
    if(SDL_PollEvent(&g->event)) {
      if(SDL_KEYDOWN == g->event.type) {
            if (g->event.key.keysym.sym == cm->Controls[CTRL_QUIT].keyTrigger ||
                g->event.key.keysym.sym == cm->Controls[CTRL_INTERACT].keyTrigger)
              return;   
      }
    }

    if(SDL_GetTicks() - Timer > 60) {
      Timer = SDL_GetTicks();
      SDL_SetRenderDrawColor(g->renderer, 0, 0, 0, 255 );
      SDL_RenderClear(g->renderer);

      SDL_Rect textplace = {(g->windowSize.w/2)-(w/2), CreditsPos - (h+5), w, h};
      SLList * iterator; int i;
      for(iterator = lm->scrollingTextures, i = 0; iterator; iterator = iterator->next, i++) {
        SDL_QueryTexture(((Resource *)GetNthElement(lm->scrollingTextures, i)->data)->texture, NULL, NULL, &w, &h);
        textplace.x = (g->windowSize.w/2)-(w/2); textplace.y += (h+5); textplace.w = w;
        SDL_RenderCopy(g->renderer, ((Resource *)GetNthElement(lm->scrollingTextures, i)->data)->texture, NULL, &textplace);
      }

      SDL_RenderPresent(g->renderer);
      CreditsPos--;
    }
  }
}

//==============================================================================
static int ScrollingExecute(SDLManager *g, ControlManager *cm, ResourceManager *rm, ListManager *lm, char ** scrollingText)
{
  lm->scrollingTextures = NULL;
  rm->AddTextsToList(rm, g, &lm->scrollingTextures, scrollingText);
  DrawScrollingAnimation(g, cm, lm);
  return 0; //success
}

//==============================================================================
static void ScrollingExit(ListManager * lm)
{
  //Mix_FreeChunk(c->scrollingsounds[0]);
  SLList * iterator;
  while((iterator = lm->scrollingTextures) != NULL) DeleteFromList(&lm->scrollingTextures, iterator);
}

//==============================================================================
void ScrollingInit(Scrolling * c)
{
  c->OnExecute = &ScrollingExecute;
  c->OnExit = &ScrollingExit;
  c->DrawScrollingAnimation = &DrawScrollingAnimation;
}

