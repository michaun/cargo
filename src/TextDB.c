/*
    This file is a part of cargo - a space shooter game
    Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "TextDB.h"

char * introText[] = {
    "<span font='16' foreground='white'><b>In the year 2209 people have colonized several solar systems.</b></span>",
    "<span font='16' foreground='white'><b>You are a captain of an intergalactic cargo ship away from your</b></span>",
    "<span font='16' foreground='white'><b>home planet Thether.</b></span>",
    "<span font='16' foreground='white'><b></b></span>",
    "<span font='16' foreground='white'><b>You were just finishing another contract when you received a cry</b></span>",
    "<span font='16' foreground='white'><b>for help. Your planet has been attacked and remains under siege</b></span>",
    "<span font='16' foreground='white'><b>People of Thether desperately need supplies only you can deliver.</b></span>",
    "<span font='16' foreground='white'><b></b></span>",
    "<span font='16' foreground='white'><b>This is your most important and most dangerous job.</b></span>",
    "<span font='16' foreground='white'><b>Break through the enemy lines and reach the airport.</b></span>",
    "<span font='16' foreground='white'><b>Fight for your family!</b></span>",
    "<span font='16' foreground='white'><b>Fight for your life!</b></span>",
    "<span font='16' foreground='white'><b>Save your planet!</b></span>",
    "<span font='16' foreground='white'><b></b></span>",
    "<span font='16' foreground='white'><b>Deliver your</b></span>",
    "<span font='16' foreground='white'><b>CARGO.</b></span>",
    (void*)0,
  };

char * creditsText[] = {
  "<span font='18' foreground='white'><b>Thank you for playing!</b></span>",
  "<span font='18' foreground='white'><b></b></span>",
  "<span font='18' foreground='white'><b>Coding:</b></span>",
  "<span font='18' foreground='white'><b>Michal Niezborala</b></span>",
  "<span font='18' foreground='white'><b></b></span>",
  "<span font='18' foreground='white'><b>Graphics:</b></span>",
  "<span font='18' foreground='white'><b>Michal Niezborala</b></span>",
  "<span font='18' foreground='white'><b>Chudy</b></span>",
  "<span font='18' foreground='white'><b></b></span>",
  "<span font='18' foreground='white'><b>This game is Free Software (GPLv3)</b></span>",
  "<span font='18' foreground='white'><b></b></span>",
  "<span font='18' foreground='white'><b>The End</b></span>",
  (void*)0,
};

  char * mission1Text[] = {
    "<span font='18' foreground='white'><b>LEVEL1</b></span>",
    "<span font='18' foreground='white'><b>BRIEFING:</b></span>",
    "<span font='18' foreground='white'><b>Your planet needs help, coordinates are in.</b></span>",
    "<span font='18' foreground='white'><b>You are flying the right direction. Go.</b></span>",
    "<span font='18' foreground='white'><b>OBJECTIVES:</b></span>",
    "<span font='18' foreground='white'><b>- Reach Thether.</b></span>",
    (void*)0,
  };
  
  char * mission2Text[] = {
    "<span font='18' foreground='white'><b>LEVEL 2</b></span>",
    "<span font='18' foreground='white'><b>BRIEFING:</b></span>",
    "<span font='18' foreground='white'><b>As you reach coordinates of Thether there is just emptiness.</b></span>",
    "<span font='18' foreground='white'><b>Something is seriously off about it all...</b></span>",
    "<span font='18' foreground='white'><b>OBJECTIVES:</b></span>",
    "<span font='18' foreground='white'><b>- Locate Thether.</b></span>",
    "<span font='18' foreground='white'><b>- Survive.</b></span>",
    (void*)0,
  };

char ** GetText(TextID ID)
{
  switch(ID) {
  case INTRO_TEXT:
    return introText;
  case CREDITS_TEXT:
    return creditsText;
  case MISSION1_BRIEFING:
    return mission1Text;
  case MISSION2_BRIEFING:
    return mission2Text;
  default:
    return (void*)0;
  }
}
