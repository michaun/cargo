/*
    This file is a part of cargo - a space shooter game
    Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//==============================================================================
#include "Title.h"
#include "CairoArt.h"
#include "ControlManager.h"
#include "ResourceManager.h"

//==============================================================================
static void DrawTitleFrame(SDLManager * g, Title * t)
{
  int refh, w, h;
  SDL_Rect placementrect;
  SDL_Rect newgamerect;
  SDL_Rect controlsrect;
  SDL_Rect exitrect;

#define PLACETEXTURE(N, X, Y)  SDL_QueryTexture(((Resource *)GetNthElement(t->lm->titleTextures, N)->data)->texture, NULL, NULL, &w, &h);  \
                               placementrect.x = X; placementrect.y = Y; placementrect.w = w; placementrect.h = h; \
                               SDL_RenderCopy(g->renderer, ((Resource *)GetNthElement(t->lm->titleTextures, N)->data)->texture, NULL, &placementrect);


  SDL_SetRenderDrawColor(g->renderer, 0, 0, 0, 255 );
  SDL_RenderClear(g->renderer);

  PLACETEXTURE(1, g->windowSize.w-w-5, g->windowSize.h-h-5); // plce version bottom right  

  PLACETEXTURE(0, (g->windowSize.w/2) - (w/2), (g->windowSize.h/2) - (h/2) - 60); // place title
  refh = placementrect.y + placementrect.h;

  PLACETEXTURE(2, (g->windowSize.w/2) - (w/2), refh + 80); // newgame
  newgamerect = placementrect;
  refh = placementrect.y + placementrect.h; 

  PLACETEXTURE(3, (g->windowSize.w/2) - (w/2), refh);  // controls
  controlsrect = placementrect;
  refh = placementrect.y + placementrect.h; 

  PLACETEXTURE(4, (g->windowSize.w/2) - (w/2), refh); // exit
  exitrect = placementrect;

  SDL_SetRenderDrawColor(g->renderer, 255, 255, 255, 255);
  if(t->currentMenuSelection == 0)
    SDL_RenderDrawRect(g->renderer, &newgamerect);
  else if (t->currentMenuSelection == 1)
    SDL_RenderDrawRect(g->renderer, &controlsrect);
  else if (t->currentMenuSelection == 2)
    SDL_RenderDrawRect(g->renderer, &exitrect);

#undef PLACETEXTURE // we don't need this shit
}

static void DrawQuestion(SDLManager *g, Title * t, Ctrl whichctrl)
{
  SDL_Rect placementrect;
  SDL_Texture * helper;
  SDL_Texture * bg;
  switch(whichctrl) {
  case CTRL_GOUP:
    helper = CreateTextTexture(g, "<span font='38' foreground='white'><b>Press a key for UP</b></span>", g->windowSize.w, 60);
    break;
  case CTRL_GODOWN:
    helper = CreateTextTexture(g, "<span font='38' foreground='white'><b>Press a key for DOWN</b></span>", g->windowSize.w, 60);
    break;
  case CTRL_GOLEFT:
    helper = CreateTextTexture(g, "<span font='38' foreground='white'><b>Press a key for LEFT</b></span>", g->windowSize.w, 60);
    break;
  case CTRL_GORIGHT:
    helper = CreateTextTexture(g, "<span font='38' foreground='white'><b>Press a key for RIGHT</b></span>", g->windowSize.w, 60);
    break;
  case CTRL_SHOOT:
    helper = CreateTextTexture(g, "<span font='38' foreground='white'><b>Press a key for SHOOT</b></span>", g->windowSize.w, 60);
    break;
  case CTRL_PAUSE:
    helper = CreateTextTexture(g, "<span font='38' foreground='white'><b>Press a key for PAUSE</b></span>", g->windowSize.w, 60);
    break;
  case CTRL_INTERACT:
    helper = CreateTextTexture(g, "<span font='38' foreground='white'><b>Press a key for INTERACT</b></span>", g->windowSize.w, 60);
    break;
  case CTRL_QUIT:
    helper = CreateTextTexture(g, "<span font='38' foreground='white'><b>Press a key for QUIT</b></span>", g->windowSize.w, 60);
    break;
  default:
    break;
  }
  
  if(helper) {
    DrawTitleFrame(g, t);
    bg = CreateOptionsBgTexture(g);
    placementrect.y = (g->windowSize.h/2);
    SDL_QueryTexture(helper, NULL, NULL, &placementrect.w, &placementrect.h);
    placementrect.x = (g->windowSize.w/2) - (placementrect.w/2);
    SDL_RenderCopy(g->renderer, bg, NULL, &placementrect);
    SDL_RenderCopy(g->renderer, helper, NULL, &placementrect);
    SDL_DestroyTexture(helper);
    SDL_DestroyTexture(bg);
  }
}

static void DrawQuestionnaire(SDLManager *g, ControlManager * cm, Title *t)
{
  char Waiting;
  //  const Uint8 *keyboard_state_array = SDL_GetKeyboardState(NULL);
  for(int i = 0; i < TOTAL_CONTROLS; i++) {
    DrawQuestion(g, t, i);
    SDL_RenderPresent(g->renderer);
    Waiting = 1;
    while(Waiting) {
      if(SDL_PollEvent(&g->event)) {
        if(SDL_KEYDOWN == g->event.type) {
          cm->ChangeControl(&cm->Controls[i], g->event.key.keysym.sym, g->event.key.keysym.scancode);
          Waiting = 0;
        }
      }
    }
  }
}

//==============================================================================
static int TitleExecute(SDLManager *g, ControlManager * cm, ResourceManager *rm, ListManager * lm, Title *t)
{
  t->lm = lm;
  
  t->coolIntroMusic = NULL;
  t->coolIntroMusic = Mix_LoadWAV( "res/intro.wav" );
  SDL_Texture * helper;
  
  helper = CreateTitleTexture(g); //g->LoadTexture(g, "res/title.png");
  rm->AddResourceToList(rm, &t->lm->titleTextures, helper, 0);
  
  SDL_SetRenderDrawColor(g->renderer, 255, 255, 255, 255 );
  helper = CreateTextTexture(g, "<span font='8' foreground='white'><b>"VERSION_STRING"</b></span>", 50, 12);
  rm->AddResourceToList(rm, &t->lm->titleTextures, helper, 0);

  helper = CreateTextTexture(g, "<span font='38' foreground='white'><b>New Game</b></span>", g->windowSize.w/2, 60);
  rm->AddResourceToList(rm, &t->lm->titleTextures, helper, 0);

  helper = CreateTextTexture(g, "<span font='38' foreground='white'><b>Controls</b></span>", g->windowSize.w/2, 60);
  rm->AddResourceToList(rm, &t->lm->titleTextures, helper, 0);

  helper = CreateTextTexture(g, "<span font='38' foreground='white'><b>Exit</b></span>", g->windowSize.w/2, 60);
  rm->AddResourceToList(rm, &t->lm->titleTextures, helper, 0);

  t->currentMenuSelection = 0;

  char TitleLoop = 1;
  char TitleChoice = 0;
  unsigned int Timer = 0;
  int ReturnCode = 0;
  Mix_PlayChannel(-1, t->coolIntroMusic, 0);
  while(TitleLoop) {
    if(SDL_PollEvent(&g->event)) {
      if(SDL_KEYDOWN == g->event.type) {
        if (g->event.key.keysym.sym == cm->Controls[CTRL_GOUP].keyTrigger)
          if(t->currentMenuSelection > 0) t->currentMenuSelection--;
        if (g->event.key.keysym.sym == cm->Controls[CTRL_GODOWN].keyTrigger)
          if(t->currentMenuSelection < 2) t->currentMenuSelection++;
        if (g->event.key.keysym.sym == cm->Controls[CTRL_QUIT].keyTrigger) {
          TitleLoop = 0; ReturnCode = 1; }
        if (g->event.key.keysym.sym == cm->Controls[CTRL_INTERACT].keyTrigger) {
          if(t->currentMenuSelection == 0) TitleChoice = 1;
          else if(t->currentMenuSelection == 1) TitleChoice = 2;
          else if(t->currentMenuSelection == 2) { TitleLoop = 0; ReturnCode = 1; }
        }
      }
    }
    if(SDL_GetTicks() - Timer > 60) {
      Timer = SDL_GetTicks();
      DrawTitleFrame(g, t);
      SDL_RenderPresent(g->renderer);
      if(TitleChoice == 1) TitleLoop = 0;
      else if(TitleChoice == 2) { DrawQuestionnaire(g, cm, t); TitleChoice = 0; }
    }
  } // TitleLoop
  
  return ReturnCode; //success
}

//==============================================================================
static void TitleExit(Title * t)
{
  
  SLList * iterator;
  
  while((iterator = t->lm->titleTextures) != NULL) DeleteFromList(&t->lm->titleTextures, iterator);
  Mix_FreeChunk(t->coolIntroMusic);
}

//==============================================================================
void TitleInit(Title * t)
{
  t->OnExecute = &TitleExecute;
  t->OnExit = &TitleExit;
  t->DrawTitleFrame = &DrawTitleFrame;
}

