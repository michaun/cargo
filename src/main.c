/*
    This file is a part of cargo - a space shooter game
    Copyright (C) 2017 Michal Niezborala <michaun _at_ protonmail [dot] com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//==============================================================================
#include "SDLManager.h"
#include "Title.h"
#include "Scrolling.h"
#include "ChapterOne.h"
#include "TextDB.h"

#include "GameStateManager.h"

#include "ListManager.h"

#include "MapsManager.h"

#include "MissileManager.h"
#include "PickableManager.h"
#include "FlyingManager.h"
#include "ExplosionManager.h"

#include <stdlib.h>
#include <time.h>

//==============================================================================
#define MIN(a, b) ((a < b) ? (a) : (b))
#define MAX(a, b) ((a > b) ? (a) : (b))

#define FLYING_SPEED 10 //this is the game's scroll speed

//==============================================================================

int main(int argc, char* argv[])
{
  srand ( time(NULL) );

  // Create and init all the managers
  SDLManager theSDLManager;
  SDLManagerInit(&theSDLManager);

  ControlManager theControlManager;
  ControlManagerInit(&theControlManager);
  theControlManager.OnExecute(&theControlManager); // this will init default game controls

  ResourceManager theResourceManager;
  ResourceManagerInit(&theResourceManager); // as of now resourcemanager does
                                            // not need to be executed/exited
  GameStateManager theGameStateManager;
  GameStateManagerInit(&theGameStateManager);

  ListManager theListManager;
  ListManagerInit(&theListManager);
  
  MissileManager theMissileManager;
  InitMissileManager(&theMissileManager);
  FlyingManager theFlyingManager;
  InitFlyingManager(&theFlyingManager, &theMissileManager); // HEADS UP: FlyingManager has to have
                                                            // a workin MissileManager ready
  ExplosionManager theExplosionManager;
  InitExplosionManager(&theExplosionManager);
  PickableManager thePickableManager;
  InitPickableManager(&thePickableManager);
  MapsManager theMapsManager;
  MapsManagerInit(&theMapsManager, &theFlyingManager, &thePickableManager, &theListManager);
  // end with managers
  
  Title theTitle;
  TitleInit(&theTitle);

  Scrolling theIntro;
  ScrollingInit(&theIntro);

  ChapterOne theChapterOne;
  ChapterOneInit(&theChapterOne);

  Scrolling theEndCredits;
  ScrollingInit(&theEndCredits);

  char ** ScrollingTextPtr;
  theGameStateManager.SetCurrentGameState(&theGameStateManager, TITLE_STATE); // error state per default
  char ReturnCode = -1; // error state on return
  //SDLManager starts first!
  ReturnCode = theSDLManager.OnExecute(&theSDLManager);
  if(ReturnCode == 0) theGameStateManager.SetCurrentGameState(&theGameStateManager, TITLE_STATE);
  
  while(theGameStateManager.GetCurrentGameState(&theGameStateManager) != ENDLOOP_STATE) { //any state should send return code "1" if they want to quit the game completely
    switch(theGameStateManager.GetCurrentGameState(&theGameStateManager)) {
    case TITLE_STATE:
      ReturnCode = theTitle.OnExecute(&theSDLManager, &theControlManager, &theResourceManager, &theListManager, &theTitle);
      if(ReturnCode == 0) theGameStateManager.SetCurrentGameState(&theGameStateManager, INTRO_STATE);
      else if(ReturnCode == 2) theGameStateManager.SetCurrentGameState(&theGameStateManager, ENDLOOP_STATE);
      else theGameStateManager.SetCurrentGameState(&theGameStateManager, ERROR_STATE);
      theTitle.OnExit(&theTitle);
      break;
    case INTRO_STATE:
      ScrollingTextPtr = GetText(INTRO_TEXT);
      ReturnCode = theIntro.OnExecute(&theSDLManager, &theControlManager, &theResourceManager, &theListManager, ScrollingTextPtr);
      if(ReturnCode == 0) theGameStateManager.SetCurrentGameState(&theGameStateManager, CHAPTERONE_STATE);
      else theGameStateManager.SetCurrentGameState(&theGameStateManager, ERROR_STATE);
      theIntro.OnExit(&theListManager);
      break;
    case CHAPTERONE_STATE:
      ReturnCode = theChapterOne.OnExecute(&theSDLManager, &theControlManager, &theResourceManager, &theMapsManager, &theListManager, &theMissileManager, &theFlyingManager, &theExplosionManager, &thePickableManager, &theChapterOne, 1);
      if(ReturnCode == 0 || ReturnCode == 2 || ReturnCode == 3) theGameStateManager.SetCurrentGameState(&theGameStateManager, QUIT_STATE);
      else theGameStateManager.SetCurrentGameState(&theGameStateManager, ERROR_STATE);
      theChapterOne.OnExit(&theListManager);
      break;
    case QUIT_STATE:
      ScrollingTextPtr = GetText(CREDITS_TEXT);
      ReturnCode = theEndCredits.OnExecute(&theSDLManager, &theControlManager, &theResourceManager, &theListManager, ScrollingTextPtr);
      if(ReturnCode == 0) theGameStateManager.SetCurrentGameState(&theGameStateManager, TITLE_STATE);
      else theGameStateManager.SetCurrentGameState(&theGameStateManager, ERROR_STATE);
      theEndCredits.OnExit(&theListManager);
      break;
    case ENDLOOP_STATE:
      /* Not handled */
      break;
    case ERROR_STATE:
      return -1;
    }
  }

  printf("all done\n");
  theControlManager.OnExit(&theControlManager); // in essence this function does nothing
                                                // but exit must what executed has been
  theSDLManager.OnExit(&theSDLManager); //SDLManager exists last!
}

//==============================================================================
